package com.purchase.rest;

import com.purchase.service.OrderService;
import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

@Controller
@RequestMapping("/order")
public class OrderController {

    @Resource
    private OrderService orderService;
    
    /**
     * 获取订单列表
     *
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public ResponseForm list(@RequestBody RequestForm param) {
        return orderService.getOrderListByStatus(param);
    }

    /**
     * 获取订单详情列表
     *
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/detailList", method = RequestMethod.POST)
    public ResponseForm detailList(@RequestBody RequestForm param) {
        return orderService.getOrderDetailList(param);
    }

    /**
     * 删除订单
     *
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public ResponseForm delete(@RequestBody RequestForm param) {
        return orderService.updateOrderIsDelete(param);
    }

    /**
     * 订单确认
     *
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/createOrderAndDetails", method = RequestMethod.POST)
    public ResponseForm createOrderAndDetails(@RequestBody RequestForm param) {
        return orderService.createOrderAndDetailsFromCart(param);
    }

    /**
     * 更新订单关联地址
     *
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/updateOrderAddress", method = RequestMethod.POST)
    public ResponseForm updateOrderAddress(@RequestBody RequestForm param) {
        return orderService.updateOrderAddress(param);
    }

    /**
     * 获取不同状态订单的数量
     *
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getOrderCount", method = RequestMethod.POST)
    public ResponseForm getOrderCount(@RequestBody RequestForm param) {
        return orderService.getOrderCountByUserId(param);
    }

    /**
     * 立即抢购
     *
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/createOrderAndDetailsRightNow", method = RequestMethod.POST)
    public ResponseForm createOrderAndDetailsRightNow(@RequestBody RequestForm param) {
        return orderService.createOrderAndDetailsRightNow(param);
    }

    /**
     * 修改订单发货状态
     *
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/updateOrderLogisticStatus", method = RequestMethod.POST)
    public ResponseForm updateOrderLogisticStatus(@RequestBody RequestForm param) {
        return orderService.updateOrderLogisticStatus(param);
    }

    /**
     * 修改详情退款标识状态
     *
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/updateDetailRefundFlag", method = RequestMethod.POST)
    public ResponseForm updateDetailRefundFlag(@RequestBody RequestForm param) {
        return orderService.updateDetailRefundFlag(param);
    }

    /**
     * 获取订单列表后台
     *
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/listForManage", method = RequestMethod.POST)
    public ResponseForm listForManage(@RequestBody RequestForm param) {
        return orderService.listForManage(param);
    }

    /**
     * 添加订单物流信息
     *
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/createLogistics", method = RequestMethod.POST)
    public ResponseForm createLogistics(@RequestBody RequestForm param) {
        return orderService.createLogistics(param);
    }

    /**
     * 获取订单物流信息
     *
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getLogistics", method = RequestMethod.POST)
    public ResponseForm getLogistics(@RequestBody RequestForm param) {
        return orderService.getLogistics(param);
    }

    /**
     * 减库存
     *
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/reduceGoodsStock", method = RequestMethod.POST)
    public ResponseForm reduceGoodsStock(@RequestBody RequestForm param) {
        return orderService.reduceGoodsStock(param);
    }

    /**
     * 取消订单
     *
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/cancel", method = RequestMethod.POST)
    public ResponseForm cancelOrder(@RequestBody RequestForm param) {
        return orderService.cancelOrder(param);
    }

    /**
     * 更新订单冗余地址
     *
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/updateOrderRedAddress", method = RequestMethod.POST)
    public ResponseForm updateOrderRedAddress(@RequestBody RequestForm param) {
        return orderService.updateOrderRedAddress(param);
    }

    /**
     * 查询订单后台
     *
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/orderForManage", method = RequestMethod.POST)
    public ResponseForm orderForManage(@RequestBody RequestForm param) {
        return orderService.orderForManage(param);
    }
    
    /**
     * @desc 399礼包订单生成创建
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/addGiftPackOrder", method = RequestMethod.POST)
    public ResponseForm addGiftPackOrder(@RequestBody RequestForm param) {
		return orderService.addGiftPackOrder(param);
    }

    /**
     * @desc 充值会员 订单(88元)
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/rechargeMemberOrder", method = RequestMethod.POST)
    public ResponseForm rechargeMemberOrder(@RequestBody RequestForm param) {
		return orderService.rechargeMemberOrder(param);
    }
    
    
//    /**
//     * @desc 查询会员优惠信息
//     * @param param
//     * @return
//     */
//    @ResponseBody
//    @RequestMapping(value = "/getMemberCouponsInfo", method = RequestMethod.POST)
//    public ResponseForm getMemberCouponsInfo(@RequestBody RequestForm param) {
//		return orderService.getMemberCouponsInfo(param);
//    }
    
}
