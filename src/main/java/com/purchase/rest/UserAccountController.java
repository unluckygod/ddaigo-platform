/**
 * *************************************
 * Copyright(c)2016 Dingdang Yixia 
 * All Rights Reserved.
 *
 * @File: UserAccountController.java
 * @Package: com.purchase.rest
 * @Description:
 * 
 * @Version: V1.0
 * @Author: Yidong.Xiang
 * @Date: 2018年6月27日 下午2:57:11
 * 
 * *************************************
 */
package com.purchase.rest;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.purchase.service.AccountHandleService;
import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

/**
 * @ClassName: UserAccountController.java
 * @Module: 个人中心-用户佣金模块
 * @Description: 
 * 
 * @author: Yidong.Xiang
 * @date: 2018年6月27日 下午2:57:11
 * 
 */
@Controller
@RequestMapping("/account")
public class UserAccountController {

	@Resource
	AccountHandleService accountHandleService;
	
	//查看个人佣金账户信息
	@ResponseBody
	@RequestMapping(value="getIncome",method=RequestMethod.POST,consumes="application/json", produces="application/json")
	public ResponseForm getIncome(@RequestBody RequestForm param)
	{
		
		return accountHandleService.getIncome(param);
	}
}
