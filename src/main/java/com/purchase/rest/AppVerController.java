package com.purchase.rest;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.purchase.mapper.IosOnlineMapper;
import com.purchase.service.AppVerService;
import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

/**
 * @ClassName: AppVerController.java
 * @Description: app版本号相关接口
 * @author: loiuhoujie
 * @date: 2018年6月1日
 */

@Controller
@RequestMapping("/app")
public class AppVerController {
	
	@Resource
	AppVerService appVerService;

	/**
	 * @param param
	 * @Description: 版本号校验
	 * @author: loiuhoujie
	 * @date: 2018年6月1日
	 */
	@ResponseBody
	@RequestMapping(value="getVerInfo",method=RequestMethod.POST)
	public ResponseForm getVerInfo(@RequestBody RequestForm param) {
		return appVerService.getVerInfo(param);
	}
	
	/**
	 * @param param
	 * @Description: 新增版本
	 * @author: loiuhoujie
	 * @date: 2018年6月1日
	 */
	@ResponseBody
	@RequestMapping(value="newVer",method=RequestMethod.POST)
	public ResponseForm newVer(@RequestBody RequestForm param) {
		return appVerService.newVer(param);
	}
	
	/**
	 * IOS端app是否上线状态强求
	 * @param null
	 * @return isOnline
	 */
	@ResponseBody
	@RequestMapping(value = "isOnline" , method = RequestMethod.POST)
	public ResponseForm isOnline() {
		return appVerService.isOnline();
	}
	
	/**
	 * IOS端游客访问 tourist
	 * @param null
	 * @return 用户信息
	 */
	@ResponseBody
	@RequestMapping(value = "touristVisit" , method = RequestMethod.POST)
	public ResponseForm touristVisit() {
		return appVerService.touristVisit();
	}
	
	@ResponseBody
	@RequestMapping(value = "appDownloadUrl")
	public String appDownloadUrl() {
		return "https://goods.dingdian.xin/asdkjfhaskjfhkajsdfhkjafhdkjlasdlhfjkaldfh.png";
	}
	
}
