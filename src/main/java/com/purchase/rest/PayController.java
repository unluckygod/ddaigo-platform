package com.purchase.rest;

import com.purchase.service.PayService;
import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/pay")
public class PayController {

    @Resource
    private PayService wxpayService;

    /**
     * 下单
     *
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/doUnifiedOrder", method = RequestMethod.POST)
    public ResponseForm doUnifiedOrder(@RequestBody RequestForm param) {
        return wxpayService.doUnifiedOrder(param);
    }

    /**
     * 399礼包下单
     * APP端
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/doUnifiedOrderForGiftPack", method = RequestMethod.POST)
    public ResponseForm doUnifiedOrderForGiftPack(@RequestBody RequestForm param) {
        return wxpayService.doUnifiedOrderForGiftPack(param);
    }
    
    /**
     * 88会员充值下单
     * 小程序
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/doUnifiedOrderForRechargeMember", method = RequestMethod.POST)
    public ResponseForm doUnifiedOrderForRechargeMember(@RequestBody RequestForm param) {
        return wxpayService.doUnifiedOrderForRechargeMember(param);
    }
    
    /**
     * 支付结果通知
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/payNotify")
    public String payNotify(HttpServletRequest request) {
        return wxpayService.payNotify(request);
    }
    
    /**
     * 礼包支付结果通知
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/payNotifyForGiftPack")
    public String payNotifyForGiftPack(HttpServletRequest request) {
        return wxpayService.payNotifyForGiftPack(request);
    }
    
    /**
     * 会员充值支付结果通知
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/payNotifyForRechargeMember")
    public String payNotifyForRechargeMember(HttpServletRequest request) {
        return wxpayService.payNotifyForRechargeMember(request);
    }

    /**
     * 查询订单
     *
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/orderQuery", method = RequestMethod.POST)
    public ResponseForm orderQuery(@RequestBody RequestForm param) {
        return wxpayService.orderQuery(param);
    }

}
