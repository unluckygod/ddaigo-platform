package com.purchase.rest;

import com.purchase.service.RefundService;
import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/refund")
public class RefundController {

    @Resource
    private RefundService refundService;

    /**
     * 申请退款
     *
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/refund", method = RequestMethod.POST)
    public ResponseForm refund(@RequestBody RequestForm param) {
        return refundService.refund(param);
    }

    /**
     * 退款结果通知
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/notify")
    public String refundNotify(HttpServletRequest request) {
        return refundService.refundNotify(request);
    }

    /**
     * 退款审核
     *
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/refundAudit", method = RequestMethod.POST)
    public ResponseForm refundAudit(@RequestBody RequestForm param) {
        return refundService.refundAudit(param);
    }

    /**
     * 退款查询
     *
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/refundQuery", method = RequestMethod.POST)
    public ResponseForm orderQuery(@RequestBody RequestForm param) {
        return refundService.refundQuery(param);
    }

}
