package com.purchase.rest;

import com.purchase.service.InviteCodeService;
import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/code")
public class InviteCodeController {

    @Autowired
    private InviteCodeService inviteCodeService;

    /**
     * 生成邀请码
     *
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/createCode", method = RequestMethod.POST)
    public ResponseForm createCode(@RequestBody RequestForm param) {
        return inviteCodeService.createCode(param);
    }

    /**
     * 查看邀请码
     *
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getCode", method = RequestMethod.POST)
    public ResponseForm getCode(@RequestBody RequestForm param) {
        return inviteCodeService.getCode(param);
    }
    
    /**
     * APP端 用户填写推广码后绑定邀请码
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value="/bindParentInviteCode" , method  = RequestMethod.POST)
    public ResponseForm bindParentInviteCode(@RequestBody RequestForm param) {
    	return inviteCodeService.bindParentInviteCode(param);
    }
    
    /**
     * 小程序端 扫码进入小程序绑定邀请码
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value="/bindInviteCodeByMiniProgram" , method  = RequestMethod.POST)
    public ResponseForm bindInviteCodeByMiniProgram(@RequestBody RequestForm param) {
    	return inviteCodeService.bindInviteCodeByMiniProgram(param);
    }
}
