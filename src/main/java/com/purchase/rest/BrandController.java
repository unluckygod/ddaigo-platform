package com.purchase.rest;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.purchase.service.AreaService;
import com.purchase.service.BrandService;
import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

@Controller
@RequestMapping("/brand")
public class BrandController {
	
	@Resource
	BrandService brandService;
	

	
	/**
	 * 获取品牌信息
	 * @param ResponseForm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getBrandList", method = RequestMethod.POST)
	public ResponseForm getBrandList(@RequestBody RequestForm param,HttpServletRequest request) {
		return brandService.getBrandList(param,request);
	}
	
	/**
	 * 添加品牌信息
	 * @param ResponseForm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/addBrand", method = RequestMethod.POST)
	public ResponseForm addBrand(@RequestBody RequestForm param,HttpServletRequest request) {
		return brandService.addBrand(param,request);
	}
	
	/**
	 * 修改品牌信息
	 * @param ResponseForm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/editBrand", method = RequestMethod.POST)
	public ResponseForm editBrand(@RequestBody RequestForm param,HttpServletRequest request) {
		return brandService.editBrand(param,request);
	}

	
	/**
	 * 根据ID获取品牌信息
	 * @param ResponseForm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getBrandById", method = RequestMethod.POST)
	public ResponseForm getBrandById(@RequestBody RequestForm param,HttpServletRequest request) {
		return brandService.getBrandById(param,request);
	}



}
