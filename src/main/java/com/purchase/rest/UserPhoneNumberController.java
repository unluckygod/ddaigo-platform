/**
 * *************************************
 * Copyright(c)2016 Dingdang Yixia 
 * All Rights Reserved.
 *
 * @File: UserPhoneNumberController.java
 * @Package: com.ddyx.rest
 * @Description:
 * 
 * @Version: V1.0
 * @Author: Yidong.Xiang
 * @Date: 2017年11月29日 下午6:18:26
 * 
 * *************************************
 */
package com.purchase.rest;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.purchase.service.IUserAccessService;
import com.purchase.service.UserService;
import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

/**
 * @ClassName: UserPhoneNumberController.java
 * @Module: 微信用户手机号请求控制器模块
 * @Description: 接收微信小程序扫码页面请求（首次授权登录请求）、微信账号登录验证请求、
 * 、			保存已授权的用户登录信息请求、获取叮店用户角色
 * 
 * @author: liuhoujie
 * @date: 2017年11月29日 下午6:18:26
 * 
 */
@Controller
@RequestMapping("/phoneno")
public class UserPhoneNumberController {

	@Resource
	private HttpServletRequest request;
	@Resource 
	IUserAccessService userAccessService;
	@Resource
	UserService userService;
	
	/**
	 * 保存用户手机号
	 * @param requestForm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/decode", method = RequestMethod.POST)
	public ResponseForm login(@RequestBody RequestForm requestForm) {
		return userAccessService.decodePhoneNumber(requestForm);
	}
	
	/**
	 * 拉取用户手机号授权
	 * @param requestForm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/pullPhone", method = RequestMethod.POST)
	public ResponseForm pullPhone(@RequestBody RequestForm requestForm) {
		return userAccessService.pullPhone(requestForm);
	}
	
	
	/**
	 * 手动绑定手机号
	 * 拉取手机号取消之后
	 * @param requestForm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/bindPhone", method = RequestMethod.POST)
	public ResponseForm bindPhone(@RequestBody RequestForm requestForm) {
		return userService.bindPhone(requestForm);
	}
}
