package com.purchase.rest;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.purchase.service.WxToolService;
import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

@Controller
@RequestMapping("/tool")
public class WxToolController {
	
	@Resource
	WxToolService wxToolService;
	
	/**
	 * @Title: getQRCode_wxProgram   
	 * @Description: 生成微信小程序的二维码   
	 * @param: @param req
	 * @param: @param resp      
	 * @return: void
	 * @author liuhoujie
	 */
	@ResponseBody
	@RequestMapping(value="getQRCode_wxProgram",method=RequestMethod.POST)
	public ResponseForm getQRCode_wxProgram(@RequestBody RequestForm requestForm) {
		return wxToolService.getQRCode_wxProgram(requestForm);
	}
}
