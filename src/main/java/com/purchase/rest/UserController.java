package com.purchase.rest;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.purchase.service.UserService;
import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;



@Controller
@RequestMapping("/user")
public class UserController {
	
	@Resource
	UserService userService;
	
	
	
	/**
	 * @Description  获取用户邮寄地址
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getUserAddress", method = RequestMethod.POST)
	public ResponseForm getUserAddress(@RequestBody RequestForm param) {
		return userService.getUserAddress(param);
	}
	
	/**
	 * @Description 新增用户邮寄地址
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/addUserAddress", method = RequestMethod.POST)
	public ResponseForm addUserAddress(@RequestBody RequestForm param) {
		return userService.addUserAddress(param);
	}
	
	/**
	 * @Description 修改用户邮寄地址
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/updateUserAddress", method = RequestMethod.POST)
	public ResponseForm updateUserAddress(@RequestBody RequestForm param) {
		return userService.updateUserAddress(param);
	}
	
	/**
	 * @Description 根据ID查询用户邮寄地址
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getUserAddressById", method = RequestMethod.POST)
	public ResponseForm getUserAddressById(@RequestBody RequestForm param) {
		return userService.getUserAddressById(param);
	}
	
	/**
	 * @Description  查询所有用户
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/listUser", method = RequestMethod.POST)
	public ResponseForm listUser(@RequestBody RequestForm param) {
		return userService.listUser(param);
	}
	
	
	/**
	 * @Description  查询仓库所有用户
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/listUserByStoreNo", method = RequestMethod.POST)
	public ResponseForm listUserByStoreNo(@RequestBody RequestForm param) {
		return userService.listUserByStoreNo(param);
	}

	/**
	 * @Description  新增用户
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/addUser", method = RequestMethod.POST)
	public ResponseForm getAllCityByPid(@RequestBody RequestForm param) {
		return userService.addUer(param);
	}
	
	/**
	 * @Description  检查用户名是否可用
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/checkUerName", method = RequestMethod.POST)
	public ResponseForm checkUerName(@RequestBody RequestForm param) {
		return userService.checkUerName(param);
	}
	
	
	/**
	 * @Description  登录
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ResponseForm login(@RequestBody RequestForm param) {
		return userService.login(param);
	}
	
	/**
	 * @Description 微信用户手机号验证
	 * @param param
	 * @return
	 * @author liuhoujie
	 */
	@ResponseBody
	@RequestMapping(value = "/checkVerifPhone", method = RequestMethod.POST)
	public ResponseForm checkVerifPhone(@RequestBody RequestForm param) {
		return userService.checkVerifyPhone(param);
	}
	
	/**
	 * @Description 下发验证码
	 * @param param
	 * @return
	 * @author liuhoujie
	 */
	@ResponseBody
	@RequestMapping(value = "/sendVerifyCode", method = RequestMethod.POST)
	public ResponseForm sendVerifyCode(@RequestBody RequestForm param) {
		return userService.sendVerifyCode(param);
	}
	
	
	/**
	 * @Description 提交验证码
	 * @param param
	 * @return
	 * @author liuhoujie
	 */
	@ResponseBody
	@RequestMapping(value = "/submitVerifyCode", method = RequestMethod.POST)
	public ResponseForm submitVerifyCode(@RequestBody RequestForm param) {
		return userService.submitVerifyCode(param);
	}
	
	
	/**
	 * @Description  修改用户信息
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/updateUser", method = RequestMethod.POST)
	public ResponseForm update(@RequestBody RequestForm param,HttpServletRequest request) {
		return userService.update(param,request);
	}
	
	/**
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getUserById", method = RequestMethod.POST)
	public ResponseForm getUserById(@RequestBody RequestForm param) {
		return userService.getUserById(param);
	}
	
	
	/**
	 * @desc 安卓端 注册 校验手机号 
	 * @param param
	 * @return
	 * @author liuhoujie
	 */
	@ResponseBody
	@RequestMapping(value = "/checkAndroidPhone" , method = RequestMethod.POST)
	public ResponseForm checkAndroidPhone(@RequestBody RequestForm param) {
		return userService.checkAndroidPhone(param);
	}
	
	/**
	 * @desc 安卓端 注册 
	 * @param param
	 * @return
	 * @author liuhoujie
	 */
	@ResponseBody
	@RequestMapping(value = "/registerAndroidUser" , method = RequestMethod.POST)
	public ResponseForm registerAndroidUser(@RequestBody RequestForm param) {
		return userService.registerAndroidUser(param);
	}
	
	/**
	 * @desc 注册即登录 (new for 小程序)
	 * @param param
	 * @return
	 * @author liuhoujie
	 */
	@ResponseBody
	@RequestMapping(value = "/register2Login" , method = RequestMethod.POST)
	public ResponseForm register2Login(@RequestBody RequestForm param) {
		return userService.register2Login(param);
	}
	
	/**
	 * @desc 安卓端 手机号密码登录 
	 * @param param
	 * @return
	 * @author liuhoujie
	 */
	@ResponseBody
	@RequestMapping(value = "/androidLoginByPwd" , method = RequestMethod.POST)
	public ResponseForm androidLoginByPwd(@RequestBody RequestForm param) {
		return userService.androidLoginByPwd(param);
	}
	
	/**
	 * @desc 安卓端 手机验证码登录 
	 * @param param
	 * @return
	 * @author liuhoujie
	 */
	@ResponseBody
	@RequestMapping(value = "/androidLoginByCode" , method = RequestMethod.POST)
	public ResponseForm androidLoginByCode(@RequestBody RequestForm param) {
		return userService.androidLoginByCode(param);
	}
	
	/**
	 * @desc 安卓端 忘记密码后通过手机验证码修改密码
	 * @param param
	 * @return
	 * @author liuhoujie
	 */
	@ResponseBody
	@RequestMapping(value = "/modifyPwdByCode" , method = RequestMethod.POST)
	public ResponseForm modifyPwdByCode(@RequestBody RequestForm param) {
		return userService.modifyPwdByCode(param);
	}
	
	/**
	 * @desc 安卓端 修改密码
	 * @param param
	 * @return
	 * @author liuhoujie
	 */
	@ResponseBody
	@RequestMapping(value = "/modifyPwd" , method = RequestMethod.POST)
	public ResponseForm modifyPwd(@RequestBody RequestForm param) {
		return userService.modifyPwd(param);
	}
	
	/**
	 * 获取推广人信息 (通过推广码)
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getInviterInfo" , method = RequestMethod.POST)
	public ResponseForm getInviterInfo(@RequestBody RequestForm param) {
		return userService.getInviterInfo(param);
	}
}
