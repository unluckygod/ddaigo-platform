package com.purchase.rest;

import com.purchase.service.BannerService;
import com.purchase.service.BrandService;
import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/banner")
public class BannerController {
	
	@Autowired
    BannerService bannerService;

	/**
	 * 获取列表
	 * @param ResponseForm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getBannerList", method = RequestMethod.POST)
	public ResponseForm getBrandList(@RequestBody RequestForm param) {
		return bannerService.getBrandList(param);
	}
	
	/**
	 * 添加信息
	 * @param ResponseForm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/addBanner", method = RequestMethod.POST)
	public ResponseForm addBanner(@RequestBody RequestForm param) {
		return bannerService.addBanner(param);
	}
	
	/**
	 * 修改信息
	 * @param ResponseForm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/editBanner", method = RequestMethod.POST)
	public ResponseForm editBanner(@RequestBody RequestForm param) {
		return bannerService.editBrand(param);
	}

	
	/**
	 * 根据ID获取信息
	 * @param ResponseForm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getBannerById", method = RequestMethod.POST)
	public ResponseForm getBannerById(@RequestBody RequestForm param) {
		return bannerService.getBannerById(param);
	}

    /**
     * 删除信息
     * @param ResponseForm
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/delBanner", method = RequestMethod.POST)
    public ResponseForm delBanner(@RequestBody RequestForm param) {
        return bannerService.delBanner(param);
    }

}
