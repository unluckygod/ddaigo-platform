package com.purchase.rest;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.purchase.service.StatisticsService;
import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;


/**
 * @ClassName: StatisticsController.java
 * @Description: 相关统计
 * @author: liuhoujie
 * @date: 2018年5月9日
 */
@Controller
@RequestMapping("/statistics")
public class StatisticsController {
	
	@Resource
	StatisticsService statisticsService;
	
	/**
	 * @Description: 订单销量与价格相关统计
	 * @author: liuhoujie
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="getSalesAndPriceData",method = RequestMethod.POST)
	public ResponseForm getSalesAndPriceData(@RequestBody RequestForm param) {
		return statisticsService.getSalesAndPriceData(param);
	}

	/**
	 * @Description: 订单信息
	 * @author: liuhoujie
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="getActivityOrderData",method = RequestMethod.POST)
	public ResponseForm getActivityOrderData(@RequestBody RequestForm param) {
		return statisticsService.getActivityOrderData(param);
	}
	
	/**
	 * @Description: 推广信息统计
	 * 根据推广人的手机号
	 * 按时间段
	 * @author: liuhoujie
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="channelInfoStatistics",method = RequestMethod.POST)
	public ResponseForm channelInfoStatistics(@RequestBody RequestForm param) {
		return statisticsService.channelInfoStatistics(param);
	}
	/**
	 * @Description: 推广用户明细
	 * 根据推广人的手机号
	 * 按时间段
	 * @author: liuhoujie
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="channelUserDetails",method = RequestMethod.POST)
	public ResponseForm channelUserDetails(@RequestBody RequestForm param) {
		return statisticsService.channelUserDetails(param);
	}
	/**
	 * @Description: 推广用户已完成订单明细
	 * 根据推广人的手机号
	 * 按时间段
	 * @author: liuhoujie
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="channelUserOrders",method = RequestMethod.POST)
	public ResponseForm channelUserOrders(@RequestBody RequestForm param) {
		return statisticsService.channelUserOrders(param);
	}

}
