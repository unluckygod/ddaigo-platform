package com.purchase.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;
//海报、活动表
@Data
@Table(name = "p_store_activity")
public class StoreActivity {
	
	@Id
	@GeneratedValue(generator = "JDBC")
	private Integer id;
	
	private String name;
	
	private String notice;
	
	private Integer type;
	
	private String imgUrl;
	
	private Integer sort;
	
	private Integer  status;
	
	private String storeNo;
	
	private Date createTime;
	
	private Date startTime;
	
	private Date endTime;
	
	private String remark;
	
	private String brandTitle;
	
	private String  brandTag;
	
	private String brandStory;
	
	@Transient
	private List imageList;
	
//	@Transient
//	private List storeList;
	
}
