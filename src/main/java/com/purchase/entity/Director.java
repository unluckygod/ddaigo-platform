package com.purchase.entity;

import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name="b_director")
public class Director {
	@Id
	@GeneratedValue(generator="JDBC")
    private Integer directorId;

    private String parentWxUid;

    private String wxUid;

    private Integer status;

    private Date createTime;

    public Integer getDirectorId() {
        return directorId;
    }

    public void setDirectorId(Integer directorId) {
        this.directorId = directorId;
    }

    public String getParentWxUid() {
        return parentWxUid;
    }

    public void setParentWxUid(String parentWxUid) {
        this.parentWxUid = parentWxUid;
    }

    public String getWxUid() {
        return wxUid;
    }

    public void setWxUid(String wxUid) {
        this.wxUid = wxUid;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}