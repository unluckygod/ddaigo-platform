package com.purchase.entity;

import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Table(name="app_user")
public class AppUser {
	@Id
	@GeneratedValue(generator="JDBC")
	private Integer userId; 
	private String wxUid;
	private String nickName;
	private String headImage;
	private String gender;
	private String city;
	private String province;
	private String language;
	private String country;
	private String mobile;
	private String unionId;
	private String openId;
	private String verifyPhone;
	private String password;
	private String inviteCode;
	private Date createTime;
	private Date latestOnlinetime;
	private String parentInviteCode;
	private Integer userStatus;
	private String personName;
	private String weixinId;
	private String alipayId;
}
