package com.purchase.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Table(name = "p_goods_image")
public class GoodsImage {
	@Id
	@GeneratedValue(generator = "JDBC")
    private Integer id;
    private Integer mediaType;
    private Integer goodsId;
    private String url;

}