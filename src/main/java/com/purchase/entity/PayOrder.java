package com.purchase.entity;

import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 流水
 */
@Data
@Table(name = "p_pay")
public class PayOrder {

    @Id
    @GeneratedValue(generator = "JDBC")
    private String id;

    private String transactionId;
    private String orderId;
    private String goodsBody;
    private BigDecimal totalFee;
    private Date createTime;
    private Date updateTime;
    private Date startTime;
    private Date expireTime;
    private String limitPay;
    private String openid;
    private String prepayId;
    private String payFailDesc;
    private String payStatus;

}