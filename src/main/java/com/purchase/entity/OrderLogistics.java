package com.purchase.entity;

import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Table(name = "p_order_logistics")
public class OrderLogistics {
	@Id
	@GeneratedValue(generator = "JDBC")
    private Integer id;

	private String orderId;

    private String logisticsName;

    private String logisticsNo;

    private Date create_time;

    private Date receivedTime;
    
	
}