package com.purchase.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;

@Data
@Table(name = "wx_user")
public class WxUser {
	
	@Id
	@GeneratedValue(generator = "JDBC")
	private Integer userId;

	private String wxUid;

	private String nickName;

	private String headImage;

	private String gender;

	private String city;

	private String province;

	private String country;

	private String language;

	private String mobile;

	private String openId;

	private String unionId;

	private String sessionKey;

	private Date createTime;
	
	private String verifyPhone;
	
	private Date latestOnlinetime;
	
	@Transient
	private String state;
	@Transient
	private String faceImage;
	// 创建人-店员的wxuid
	@Transient
	private String createUser;
	
	private String inviteCode;
	
	private String password;
	
	private BigDecimal purchaseCost;
	
	private Integer userStatus;
	
	private String parentWxUid;

	public WxUser(String verifyPhone) {
		this.verifyPhone = verifyPhone;
	}



	public WxUser() {
	}
	
	
}