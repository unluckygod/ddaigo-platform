package com.purchase.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;

@Data
@Table(name = "p_store")
public class Store {
	@Id
	@GeneratedValue(generator = "JDBC")
    private Integer storeId;

	private String storeNo;

    private String storeName;

    private String location;

    private String phoneNumber;

    private String storeIntro;

    private Date createTime;

    private Date updateTime;
    
    private String remark;
    
    private BigDecimal postage;
    
    @Transient
	private List barndList;
	
}