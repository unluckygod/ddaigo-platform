package com.purchase.entity;

import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;

//用戶表
@Data
@Table(name = "p_user")
public class User {

	@Id
	@GeneratedValue(generator = "JDBC")
	
	private Integer id;
	
	private String name ;
	
	private String password;
	
	private Integer roleId;
	
	private Date createTime;
	
	private Integer status;
	
	private String nickName;
	
	private String url;
	
	private String storeNo;
	
	@Transient
	private String sessionId;
	
}
