package com.purchase.entity;

import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 退款订单
 */
@Data
@Table(name = "p_refund_order")
public class RefundOrder {

    @Id
    @GeneratedValue(generator = "JDBC")
    private String id;

    private String orderId;

    private String payOrderId;

    private BigDecimal refundPrice;

    private String refundDesc;

    private BigDecimal orderPrice;

    private Date createTime;

    private Date updateTime;

    private String refundFlag;

    private String refundOrderStatus;

}
