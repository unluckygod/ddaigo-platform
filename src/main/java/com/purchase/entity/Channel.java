package com.purchase.entity;

import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Table(name = "p_channel")
public class Channel {
    @Id
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    private String channel;
    
    private Date createTime;
    
    private Integer status;
    
    private String channelName;

   

  
}
