package com.purchase.entity;

import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;

@Data
@Table(name = "p_coupon")
public class Coupon {
    @Id
    @GeneratedValue(generator = "JDBC")
    private Integer id;
    private String name;
    private Integer type;
    private Integer discount;
    private Integer couponAmount;
    private Integer sillAmount;
    private Date startTime;
    private Date endTime;
    private Date releaseTime;
    private Date createTime;
    private Date updateTime;
    private String detail;
    private Integer status;
    private Integer memberFlag;
    
    @Transient
    private Integer isReceived;
    @Transient
    private Integer isUsed;
    @Transient
    private Integer orderAble;
}
