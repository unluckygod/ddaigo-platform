package com.purchase.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

//地区码表
@Data
@Table(name = "p_area")
public class Area {
	@Id
	@GeneratedValue(generator = "JDBC")
	private Integer id;

	private Integer pid;

	private String name;

	private String shortname;

	private String longitude;

	private String latitude;

	private Integer level;

	private Integer sort;

	private Integer status;

	// @Transient
	// private List goodsList;

}
