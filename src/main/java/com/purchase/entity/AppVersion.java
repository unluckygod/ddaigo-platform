package com.purchase.entity;

import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Table(name = "app_version")
public class AppVersion {
	@Id
	@GeneratedValue(generator="JDBC")
	private Integer id;
	private String appName;
	private String appAlias;
	private String appVer;
	private String appSize;
	private String appDevice;
	private String releaseDesc;
	private Date releaseTime;
	private String verCode;
	private String appLink;
}
