package com.purchase.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;

@Data
@Table(name = "p_goods")
public class Goods {
	@Id
	@GeneratedValue(generator = "JDBC")
	// @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer goodsId;

	private Integer categoryId;

	private Integer brandId;

	private String goodsCode;

	private String relation;

	private String goodsName;

	private String goodsPic;

	private String goodsDesc;

	private String goodsColor;

	private BigDecimal goodsPrice;

	private Date soldonTime;

	private Date soldoutTime;

	private Integer soldout;

	private String storeNo;

	private String discountRate;

	private BigDecimal discountPrice;

	private Date discountTime;

	private String goodsDetail;

	private String purchaseCost;

	private BigDecimal memberRate;
	
	private BigDecimal memberPrice;
	
	private Integer giftPack;
	
	private Integer hotSale;
	
	private Date presaleTime;
	
	// @Transient
	// private Integer bookingCount;
	// 图片
	@Transient
	private List<GoodsImage> goodsImages;
	// 颜色
	@Transient
	private List<Map<String, Object>> goodsColour;
	// 尺寸
	@Transient
	private List<GoodsStock> goodsStock;

	@Transient
	private List paramList;

	@Transient
	private String brandName;

	@Transient
	private Integer activityStatus;

	@Transient
	private Integer activityId;
	
	@Transient
	private Date startTime;
	
	@Transient
	private Date endTime;
	
}