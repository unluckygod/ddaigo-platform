package com.purchase.entity;

import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 订单
 */
@Data
@Table(name = "p_order")
public class Order {

    @Id
    @GeneratedValue(generator = "JDBC")
    private String id;

    private String wxUid;

    private BigDecimal price;

    private Integer goodsNum;

    private Integer userAddressId;

    private String storeNo;

    private Date createTime;

    private Date updateTime;

    private Date expireTime;

    private Integer isDelete;

    private String orderStatus;

    private String logisticStatus;

    private BigDecimal postage;

    private String contacts;

    private String mobilePhone;

    private String detailedAddress;

    private Integer activityId;

    private String tradeType;
    
    private String orderType;
    
    private BigDecimal discountAmount;
    
    private String memberCouponsId;

}