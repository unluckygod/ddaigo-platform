package com.purchase.util;

import java.io.Serializable;
import java.util.Map;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

/**
 * @ClassName: MQUtil.java
 * @Description: ActiveMq发送工具,负责被调用,接收传送过来的数据,然后发布给订阅了该队列的消费者
 * @author: LHJ
 * @date: 2017年12月27日
 */
public class MQUtil {
	// 用于 事件同步
	public static void sendObMsg(final Map<String, Object> paramMap, JmsTemplate jmsTemplate, Destination destination) {
		if (paramMap != null) {
			jmsTemplate.send(destination, new MessageCreator() {
				@Override
				public Message createMessage(Session session) throws JMSException {
					ObjectMessage obMsg = session.createObjectMessage();
					obMsg.setObject((Serializable) paramMap);
					return obMsg;
				}
			});
		}
	}

	// 用于发送 模版消息
	public static void sendJsonMsg(final String tmpMsgParam, JmsTemplate jmsTemplate, Destination destination) {
		if (StringUtils.isNotBlank(tmpMsgParam) && jmsTemplate != null && destination != null) {
			jmsTemplate.send(destination, new MessageCreator() {
				@Override
				public Message createMessage(Session session) throws JMSException {
					TextMessage tMsg = session.createTextMessage(tmpMsgParam);
					return tMsg;
				}
			});
		}
	}
}
