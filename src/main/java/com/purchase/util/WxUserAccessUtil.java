/**
 * *************************************
 * Copyright(c)2016 Dingdang Yixia 
 * All Rights Reserved.
 *
 * @File: WXAppletUserInfo.java
 * @Package: com.ddyx.service
 * @Description:
 * 
 * @Version: V1.0
 * @Author: Yidong.Xiang
 * @Date: 2017年12月10日 下午1:14:30
 * 
 * *************************************
 */
package com.purchase.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.security.AlgorithmParameters;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.security.spec.InvalidParameterSpecException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.ResourceBundle;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.log4j.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.codehaus.xfire.util.Base64;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;


/**
 * @ClassName: WxUserAccessServiceImpl.java
 * @Module: 
 * @Description: 
 * 
 * @author: liuhoujie
 * @date: 2017年12月10日 下午1:14:30
 * 
 */
public class WxUserAccessUtil {
	private static Logger log = Logger.getLogger(WxUserAccessUtil.class);
	
    /**
     * 使用登录凭证 code 获取 session_key 和 openid
     * 
     * @param code 调用微信登陆返回的Code
     * @return	JSONObject 	session_key和openid的JSON对象
     */
	public static JSONObject getSessionKeyOropenid(String code, String storeNo) {
		// 微信端登录code值
		String wxCode = code;
		String appId = null;
		String appSecret = null;
		// dingdian appId/appSecret
		appId = "wx5480a2b0b0cbdba2";
		appSecret = "53eb995aaadc951ff2e724749e102d2f";

		// String authCode ="xKGo080XYDm2e^HFJin51DV8$";
		String authCode = "authorization_code";
		String requestUrl = "https://api.weixin.qq.com/sns/jscode2session";
		Map<String, String> requestUrlParam = new HashMap<String, String>();
		requestUrlParam.put("appid", appId); // 开发者设置中的appId
		requestUrlParam.put("secret", appSecret); // 开发者设置中的appSecret
		requestUrlParam.put("js_code", wxCode); // 小程序调用wx.login返回的code
		requestUrlParam.put("grant_type", authCode); // 默认参数

		// ResourceBundle resource = ResourceBundle.getBundle("weixin"); //读取属性文件
		// String requestUrl = resource.getString("url"); //请求地址
		// https://api.weixin.qq.com/sns/jscode2session
		// requestUrlParam.put("appid", resource.getString("appId")); //开发者设置中的appId
		// requestUrlParam.put("secret", resource.getString("appSecret"));
		// //开发者设置中的appSecret
		// requestUrlParam.put("js_code", wxCode); //小程序调用wx.login返回的code
		// requestUrlParam.put("grant_type", "authorization_code"); //默认参数
		// 发送post请求读取调用微信 https://api.weixin.qq.com/sns/jscode2session 接口获取openid用户唯一标识
		JSONObject jsonObject = JSON.parseObject(sendPost(requestUrl, requestUrlParam));
		return jsonObject;
	}

    /**
     * 解密用户敏感数据获取用户信息
     * 
     * @param sessionKey 数据进行加密签名的密钥
     * @param encryptedData 包括敏感数据在内的完整用户信息的加密数据
     * @param iv 加密算法的初始向量
     * @return
     */
    public static JSONObject getUserInfo(String encryptedData,String sessionKey,String iv){
        // 被加密的数据
        byte[] dataByte = Base64.decode(encryptedData);
        // 加密秘钥
        byte[] keyByte = Base64.decode(sessionKey);
        // 偏移量
        byte[] ivByte = Base64.decode(iv);
        try {
               // 如果密钥不足16位，那么就补足.  这个if 中的内容很重要
            int base = 16;
            if (keyByte.length % base != 0) {
                int groups = keyByte.length / base + (keyByte.length % base != 0 ? 1 : 0);
                byte[] temp = new byte[groups * base];
                Arrays.fill(temp, (byte) 0);
                System.arraycopy(keyByte, 0, temp, 0, keyByte.length);
                keyByte = temp;
            }
            // 初始化
            Security.addProvider(new BouncyCastleProvider());
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding","BC");
            SecretKeySpec spec = new SecretKeySpec(keyByte, "AES");
            AlgorithmParameters parameters = AlgorithmParameters.getInstance("AES");
            parameters.init(new IvParameterSpec(ivByte));
            cipher.init(Cipher.DECRYPT_MODE, spec, parameters);// 初始化
            byte[] resultByte = cipher.doFinal(dataByte);
            if (null != resultByte && resultByte.length > 0) {
                String result = new String(resultByte, "UTF-8");
                return JSON.parseObject(result);
            }
        } catch (NoSuchAlgorithmException e) {
            log.error(e.getMessage(), e);
        } catch (NoSuchPaddingException e) {
        	log.error(e.getMessage(), e);
        } catch (InvalidParameterSpecException e) {
        	log.error(e.getMessage(), e);
        } catch (IllegalBlockSizeException e) {
        	log.error(e.getMessage(), e);
        } catch (BadPaddingException e) {
        	log.error(e.getMessage(), e);
        } catch (UnsupportedEncodingException e) {
        	log.error(e.getMessage(), e);
        } catch (InvalidKeyException e) {
        	log.error(e.getMessage(), e);
        } catch (InvalidAlgorithmParameterException e) {
        	log.error(e.getMessage(), e);
        } catch (NoSuchProviderException e) {
        	log.error(e.getMessage(), e);
        }
        return null;
    }
    
    /**
	 * 向指定 URL 发送POST方法的请求
	 * 
	 * @param url 发送请求的 URL
	 * @param param 请求参数
	 * @return 所代表远程资源的响应结果
	 */
	public static String sendPost(String url, Map<String, ?> paramMap) {
	    PrintWriter out = null;
	    BufferedReader in = null;
	    String result = "";
	    
	    String param = "";
		Iterator<String> it = paramMap.keySet().iterator();
		
		while(it.hasNext()) {
			String key = it.next();
			param += key + "=" + paramMap.get(key) + "&";
		}
		
	    try {
	        URL realUrl = new URL(url);
	        // 打开和URL之间的连接
	        URLConnection conn = realUrl.openConnection();
	        // 设置通用的请求属性
	        conn.setRequestProperty("accept", "*/*");
	        conn.setRequestProperty("connection", "Keep-Alive");
	        conn.setRequestProperty("Accept-Charset", "utf-8");
	        conn.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
	        // 发送POST请求必须设置如下两行
	        conn.setDoOutput(true);
	        conn.setDoInput(true);
	        // 获取URLConnection对象对应的输出流
	        out = new PrintWriter(conn.getOutputStream());
	        // 发送请求参数
	        out.print(param);
	        // flush输出流的缓冲
	        out.flush();
	        // 定义BufferedReader输入流来读取URL的响应
	        in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
	        String line;
	        while ((line = in.readLine()) != null) {
	            result += line;
	        }
	    } catch (Exception e) {
	    	log.error(e.getMessage(), e);
	    }
	    //使用finally块来关闭输出流、输入流
	    finally{
	        try{
	            if(out!=null){
	                out.close();
	            }
	            if(in!=null){
	                in.close();
	            }
	        }
	        catch(IOException ex){
	            ex.printStackTrace();
	        }
	    }
	    return result;
	}	
    
}
