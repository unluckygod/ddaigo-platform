package com.purchase.util;

/**
 * 订单号生成器
 */
public class OrderIdGenerater {

    public static String generatePayOrderId() {
        SnowflakeIdWorker snowflakeIdWorker = new SnowflakeIdWorker(1, 1);
        return "kc" + snowflakeIdWorker.nextId();
    }

    public static String generateOrderId() {
        SnowflakeIdWorker snowflakeIdWorker = new SnowflakeIdWorker(1, 2);
        return String.valueOf(snowflakeIdWorker.nextId());
    }

    public static String generateRefundPayOrderId() {
        SnowflakeIdWorker snowflakeIdWorker = new SnowflakeIdWorker(2, 1);
        return "rf" + snowflakeIdWorker.nextId();
    }

    public static String generateRefundOrderId() {
        SnowflakeIdWorker snowflakeIdWorker = new SnowflakeIdWorker(2, 2);
        return String.valueOf(snowflakeIdWorker.nextId());
    }

}
