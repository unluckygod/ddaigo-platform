package com.purchase.util;

import com.purchase.mapper.CartMapper;
import com.purchase.mapper.OrderMapper;
import com.purchase.mapper.StoreActivityMapper;
import com.purchase.mapper.TimeHandlerMapper;

import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @ClassName: TimeHandlerUtil.java
 * @Description: 定时处理所有已过期数据
 * @author: liuhoujie
 * @date: 2018年1月17日
 */
@Component
public class TimeHandlerUtil {

	@Resource
	private StoreActivityMapper saMapper;
	@Resource
	private CartMapper cartMapper;
	@Resource
	private OrderMapper orderMapper;
	
	@Resource
	TimeHandlerMapper timeHandlerMapper;
	
	/**
	 * 定时活动状态切换
	 * 每1min
	 */
	@Lazy(true)
	@Scheduled(cron = "0 0/1 * * * ?")
	public void autoSwitchActivitiesStatus() {
		// System.out.println("---------------活动状态切换---------------------");
		timeHandlerMapper.autoSwitchActivitiesStatus();
	}

	/**
	 * 定期删除失效购物车订单
	 * 每1min
	 */
	@Lazy(true)
	@Scheduled(cron = "0 0/1 * * * ?")
	public void cleanInvalidCarts() {
		timeHandlerMapper.cleanInvalidCarts();
	}

	/**
	 * 订单状态更换
	 * 每1s
	 */
	@Lazy(true)
	@Scheduled(cron = "0/1 * * * * ?")
	public void autoSwitchOrderStatus() {
		timeHandlerMapper.autoSwitchOrderStatus();
	}

	/**
	 * 订单完成状态(已完成/确认收货)
	 * 每1m
	 */
	@Lazy(true)
	@Scheduled(cron = "0 0/1 * * * ?")
	public void receivedOrdersStatus() {
		timeHandlerMapper.receivedOrdersStatus();
	}

}
