package com.purchase.util;

import java.util.Random;

/**
 * 邀请码生成
 */
public class InviteCodeUtil {

    private static final String[] storeInvitationChars = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P"
            , "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};

    /**
     * 生成六位邀请码
     * @param place 创建几位邀请码
     * @return
     */
    public static String generateCode(int place) {
        StringBuilder randomStr = new StringBuilder();
        for (int i = 0; i < place; i++) {
            Random random = new Random();
            int randomIndex = random.nextInt(36);
            randomStr.append(storeInvitationChars[randomIndex]);
        }
        return String.valueOf(randomStr);
    }

    
    /**
     * APP 用户生成六位推广码
     * @param data 加密的内容(手机号后三位)
     * @param key 加密的规则(wxUid)
     * @return
     */
    public static String generateAppInviteCode(String data,String key) {
    	return RC4.encry_RC4_string(data, key);
    }
    
    public static void main(String[] args) {
//        for (int i = 0; i < 100000; i++) {
//            System.out.println(InviteCodeUtil.generateCode(6));
//        }
    	String generateAppInviteCode = generateAppInviteCode("556", "ca04965b-48ed-4d2d-8936-895aad77910f");
    	System.out.println(generateAppInviteCode);
    }

}
