package com.purchase.util;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

public class ResponseForm implements Serializable {

	private static final long serialVersionUID = 1L;
	private boolean status = true;
	private String code = "200";
	private String message = LogInfo.SUCCESS;
	private Integer total = null; // 数据总数

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	private Object data = null;

	public void setStatus(boolean status) {
		this.status = status;
	}

	public boolean getStatus() {
		return this.status;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public void succ(String message, Object data) {
		this.code = "200";
		this.message = StringUtils.isBlank(message) ? "操作成功" : message;
		this.status = true;
		if (data != null)
			this.data = data;
	}
	public void succ(String message) {
		this.code = "200";
		this.message = StringUtils.isBlank(message) ? "操作成功" : message;
		this.status = true;
	}


	public void fail(String code, String message, Object data) {
		this.code = StringUtils.isBlank(code) ? "400" : code;
		this.message = StringUtils.isBlank(message) ? "操作错误" : message;
		this.status = false;
		if (data != null)
			this.data = data;
	}

	public void fail(String code, String message) {
		this.code = StringUtils.isBlank(code) ? "400" : code;
		this.message = StringUtils.isBlank(message) ? "操作错误" : message;
		this.status = false;
	}
}
