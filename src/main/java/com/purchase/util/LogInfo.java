package com.purchase.util;

public class LogInfo {

	public static final String ERROR = "操作错误";
	public static final String AUTH_ERROR = "认证错误";
	public static final String USER_NAME_LOG = "用户名不能为空";
	public static final String USER_LOG = "用户不存在";
	public static final String USER_PASSWORD_LOG = "密码不能为空";
	public static final String USER_CHECK_LOG = "用户名已存在";
	public static final String USER_CHECK_SUCCESS = "用户名可用";
	public static final String SUCCESS = "操作成功";
	public static final String PARAM_ERROR = "请检查参数";
	public static final String USER_ERROR = "用户不存在";
	public static final String PASSWORD_ERROR = "请检查密码";
	
	
	//code
	public static final String SUCCESS_CODE = "1";
	//code
	public static final String ERROR_CODE = "0";
		
	

}
