package com.purchase.util;


public class NoUtil {
	
	/**
	 * @DESC 生成门店编号,共9位 
	 * @author liuhoujie
	 * @param storeNo
	 * @return
	 */
	public static synchronized  String storeNoUil(String storeNo) {
		String subStr = storeNo.substring(1);
		Integer num = Integer.valueOf(subStr);
		num++;
		String numStr = String.valueOf(num);
		storeNo = "S";
		for (int i = 0; i < 8-numStr.length(); i++) {
			storeNo+="0";
		}
		return storeNo+numStr; 
	}
	
}
