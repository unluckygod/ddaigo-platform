package com.purchase.service;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

public interface OrderService {

    /**
     * 获取订单列表
     *
     * @param param
     * @return
     */
    ResponseForm getOrderListByStatus(RequestForm param);

    /**
     * 获取订单详情
     *
     * @param param
     * @return
     */
    ResponseForm getOrderDetailList(RequestForm param);

    /**
     * 删除订单
     *
     * @param param
     * @return
     */
    ResponseForm updateOrderIsDelete(RequestForm param);

    /**
     * 更新订单关联地址
     *
     * @param param
     * @return
     */
    ResponseForm updateOrderAddress(RequestForm param);

    /**
     * 生成订单和详情购物车用
     *
     * @param param
     * @return
     */
    ResponseForm createOrderAndDetailsFromCart(RequestForm param);

    /**
     * 立即抢购
     *
     * @param param
     * @return
     */
    ResponseForm createOrderAndDetailsRightNow(RequestForm param);

    /**
     * 获取不同状态订单数量
     *
     * @param param
     * @return
     */
    ResponseForm getOrderCountByUserId(RequestForm param);

    /**
     * 修改订单发货状态
     *
     * @param param
     * @return
     */
    ResponseForm updateOrderLogisticStatus(RequestForm param);

    /**
     * 修改详情退款标识状态
     *
     * @param param
     * @return
     */
    ResponseForm updateDetailRefundFlag(RequestForm param);

    /**
     * 获取订单列表后台
     *
     * @param param
     * @return
     */
    ResponseForm listForManage(RequestForm param);

    /**
     * 添加物流信息
     *
     * @param param
     * @return
     */
    ResponseForm createLogistics(RequestForm param);

    /**
     * 获取订单物流信息
     *
     * @param param
     * @return
     */
    ResponseForm getLogistics(RequestForm param);

    /**
     * 减库存
     *
     * @param param
     * @return
     */
    ResponseForm reduceGoodsStock(RequestForm param);

    /**
     * 取消订单
     * @param param
     * @return
     */
    ResponseForm cancelOrder(RequestForm param);

    /**
     * 更新订单冗余地址
     *
     * @param param
     * @return
     */
    ResponseForm updateOrderRedAddress(RequestForm param);

    /**
     * 查询订单后台
     * @param param
     * @return
     */
    ResponseForm orderForManage(RequestForm param);

    /**
     * @desc 399礼包订单生成创建
     * @param param
     * @return
     */
	ResponseForm addGiftPackOrder(RequestForm param);

	/**
     * @desc 充值会员 订单(88元)
     * @param param
     * @return
     */
	ResponseForm rechargeMemberOrder(RequestForm param);

//	ResponseForm getMemberCouponsInfo(RequestForm param);
}
