package com.purchase.service;

import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

public interface ForwardService {

	ResponseForm saveForward(RequestForm param);

}
