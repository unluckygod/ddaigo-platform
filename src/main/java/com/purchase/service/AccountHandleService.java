/**
 * *************************************
 * Copyright(c)2016 Dingdang Yixia 
 * All Rights Reserved.
 *
 * @File: AccountService.java
 * @Package: com.purchase.service
 * @Description:
 * 
 * @Version: V1.0
 * @Author: Yidong.Xiang
 * @Date: 2018年6月28日 下午2:47:50
 * 
 * *************************************
 */
package com.purchase.service;

import java.math.BigDecimal;

import com.alibaba.fastjson.JSONObject;
import com.purchase.entity.AppUser;
import com.purchase.entity.Order;
import com.purchase.entity.WxUser;
import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

/**
 * @ClassName: AccountService.java
 * @Module: 佣金业务逻辑模块
 * @Description: 
 * 
 * @author: Yidong.Xiang
 * @date: 2018年6月27日 下午2:47:50
 * 
 */
public interface AccountHandleService {

	/**
	 * 获取个人佣金账户信息
	 * @param wxUid	微信用户ID	
	 * @return
	 */
	public ResponseForm getIncome(RequestForm param);
	
	/**
	 * 获取用户的佣金明细
	 * @param wxUid
	 * @return
	 */
	public JSONObject getIncomeDetails(String wxUid);
	/**
	 * 创建APP端代购佣金账户，购买399礼包支付成功后系统分配一个佣金账户
	 * @return
	 */
	public int createVendorAccount(AppUser appUser);
	/**
	 * 创建小程序端会员佣金账户，购买88礼包支付成功后系统分配一个佣金账户，（和代购用户不在一个体系中）
	 * @param wxUid
	 * @param parentWxUid
	 * @param orderId
	 * @return
	 */
	public int createMemberAccount(String wxUid,String parentWxUid,String orderId);
	
	/**
	 * 购买佣金提成计算，用户下单支付成功后，按规则分配佣金
	 * @param wxUid		下单用户ID
	 * @param orderId	订单ID
	 * @param tradeType	订单类型（小程序端下单或app端下单）
	 * @return
	 */
	public int createBonus(String wxUid,String orderId,String tradeType);
	
	/**
	 * 推荐代购佣金提成计算，购买399礼包成功后，按规则分配奖金
	 * @param appUser
	 * @param orderId
	 * @return
	 */
	public int createVendorAward(AppUser appUser,String orderId);
	/**
	 * 推荐会员佣金提成计算，购买88礼包成功后，按规则分配奖金
	 * @param wxUid
	 * @param parentWxUid
	 * @param orderId
	 * @return
	 */
	public int createMemberAward(String wxUid,String parentWxUid,String orderId);
}
