package com.purchase.service;

import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

public interface BannerService {
    ResponseForm getBrandList(RequestForm param);

    ResponseForm addBanner(RequestForm param);

    ResponseForm editBrand(RequestForm param);

    ResponseForm getBannerById(RequestForm param);

    ResponseForm delBanner(RequestForm param);

}
