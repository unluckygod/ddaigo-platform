package com.purchase.service;

import javax.servlet.http.HttpServletRequest;

import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

public interface UserService {

	//添加用户
	ResponseForm addUer(RequestForm param);

	//检查用户名是否可用
	ResponseForm checkUerName(RequestForm param);
	
	//用户登录
	ResponseForm login(RequestForm param);

	//修改用户信息
	ResponseForm update(RequestForm param,HttpServletRequest request);

	ResponseForm getUserAddress(RequestForm param);

	ResponseForm addUserAddress(RequestForm param);

	ResponseForm updateUserAddress(RequestForm param);

	ResponseForm getUserAddressById(RequestForm param);

	ResponseForm listUser(RequestForm param);

	ResponseForm getUserById(RequestForm param);

	ResponseForm listUserByStoreNo(RequestForm param);

	ResponseForm sendVerifyCode(RequestForm param);

	ResponseForm submitVerifyCode(RequestForm param);

	ResponseForm checkVerifyPhone(RequestForm param);

	ResponseForm checkAndroidPhone(RequestForm param);

	ResponseForm registerAndroidUser(RequestForm param);

	ResponseForm androidLoginByPwd(RequestForm param);

	ResponseForm androidLoginByCode(RequestForm param);

	ResponseForm modifyPwdByCode(RequestForm param);

	ResponseForm modifyPwd(RequestForm param);

	ResponseForm register2Login(RequestForm param);

	ResponseForm bindPhone(RequestForm requestForm);

	ResponseForm getInviterInfo(RequestForm param);
	
}
