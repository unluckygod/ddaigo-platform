package com.purchase.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.purchase.entity.Channel;
import com.purchase.mapper.ChannelMapper;
import com.purchase.service.ChannelService;
import com.purchase.util.LogInfo;
import com.purchase.util.MyBeanUtils;
import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ChannelServiceImpl implements ChannelService {

	@Resource
	ChannelMapper channelMapper;

	@Override
	public ResponseForm channelList(RequestForm param) {
		ResponseForm result = new ResponseForm();
		Map<String, Object> paramMap = (Map<String, Object>) param.getData();
		try {
			
//			Example example = new Example(Channel.class);
//			example.setOrderByClause("status DESC,create_time DESC");
			com.purchase.util.Page.parsePage(paramMap);
			Page<Object> pageHelper = PageHelper.startPage((int) paramMap.get("page"), (int) paramMap.get("size"));
			List<Channel> channelList=channelMapper.channelList(paramMap);
			result.setTotal((int) pageHelper.getTotal());
			result.setData(channelList);
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error(LogInfo.ERROR);
		}
		return result;
	}

	@Override
	public ResponseForm addChannel(RequestForm param) {
		ResponseForm result = new ResponseForm();
		Map<String, Object> paramMap = (Map<String, Object>) param.getData();
		try {
			
			String channel =(String) paramMap.get("channel");
			if(StringUtils.isBlank(channel)){
				result.setStatus(false);
				result.setCode("400");
				result.setMessage(LogInfo.PARAM_ERROR);
				log.error(LogInfo.PARAM_ERROR);
				return result;
			}
			
			Channel channelBean = new Channel();
			channelBean.setChannel(channel);
			channelBean = channelMapper.selectOne(channelBean);
			if(channelBean!=null) {
				result.setStatus(false);
				result.setCode("400");
				result.setMessage("重复的渠道号,请更换!");
				log.error("重复的渠道号,请更换!");
				return result;
			}
			channelBean = new Channel();
			MyBeanUtils.populate(channelBean, paramMap);
			channelMapper.insertSelective(channelBean);
			result.setCode("200");
		} catch (Exception e) {
			e.printStackTrace();
			result.setCode("500");
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error(LogInfo.ERROR);
		}
		return result;
	}

	@Override
	public ResponseForm editChannel(RequestForm param) {
		ResponseForm result = new ResponseForm();
		Map<String, Object> paramMap = (Map<String, Object>) param.getData();
		try {
			
			String channel =(String) paramMap.get("channel");
			if(!StringUtils.isNotBlank(channel)){
				result.setStatus(false);
				result.setMessage(LogInfo.PARAM_ERROR);
				log.error(LogInfo.PARAM_ERROR);
				return result;
			}
			
			Channel channelBean = new Channel();
			MyBeanUtils.populate(channelBean, paramMap);
			channelMapper.updateByPrimaryKeySelective(channelBean);
			
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error(LogInfo.ERROR);
		}
		return result;
	}

	@Override
	public ResponseForm getChannel(RequestForm param) {
		ResponseForm result = new ResponseForm();
		Map<String, Object> paramMap = (Map<String, Object>) param.getData();
		try {
			
			String id =(String) paramMap.get("id");
			if(!StringUtils.isNotBlank(id)){
				result.setStatus(false);
				result.setMessage(LogInfo.PARAM_ERROR);
				log.error(LogInfo.PARAM_ERROR);
				return result;
			}
			
			Channel channel=channelMapper.selectByPrimaryKey(Integer.parseInt(id));
			result.setData(channel);
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error(LogInfo.ERROR);
		}
		return result;
	}
	
	@SuppressWarnings({ "unused", "unchecked", "rawtypes" })
	@Override
	public ResponseForm checkChannelpeople(RequestForm param) {
		ResponseForm result = new ResponseForm();
		if (param.getData() != null) {
			Map paramMap = (Map) param.getData();
			String wxUid = (String) paramMap.get("wxUid");
			try {
				Integer isChannelPeople = channelMapper.checkChannelpeople(wxUid);
				Map resultMap = new HashMap<>();
				resultMap.put("isChannelPeople", isChannelPeople);
				result.setData(resultMap);
				result.setCode("200");
				result.setStatus(true);
				result.setMessage("操作成功");
			} catch (Exception e) {
				e.printStackTrace();
				result.setStatus(false);
				result.setCode("500");
				result.setMessage("请求错误");
				log.error("请求错误");
			}
		} else {
			result.setCode("500");
			result.setStatus(false);
			result.setMessage("参数不为空或缺少必要参数");
		}
		return result;
	}
}
