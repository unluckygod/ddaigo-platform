package com.purchase.service.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.http.client.ClientProtocolException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.purchase.service.WxToolService;
import com.purchase.util.QRCode_wxProgram;
import com.purchase.util.QiniuUtil;
import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;
import com.purchase.util.Utils;

@Service
public class WxToolServiceImpl implements WxToolService{

	@Override
	public ResponseForm getQRCode_wxProgram(RequestForm param) {
		ResponseForm result = new ResponseForm();
		if (param.getData() != null) {
			Map<String, Object> paramMap = (Map<String, Object>) param.getData();
			Map<String, Object> map = new HashMap<>();
			String page=paramMap.get("page")+"";
			String scene=paramMap.get("scene")+"";
			String width="10000";
			String imgName=Utils.getUUID();  
			
			
			//String page="pages/home/home";//你二维码中跳向的地址
			//String scene="B02S0002,B0000001,12";
		    //map.put("page", "pages/product/product");
		    //map.put("width", "230");//图片大小
		    //scene 最多支持 20个 参数
		    //map.put("scene", "B02S0002,B0000001,12");
			try {
				//获取二维码图片
				String localFilePath=new QRCode_wxProgram().GetUrl(page, scene, width, imgName);
				if(!StringUtils.hasText(localFilePath)) {
					result.setStatus(false);
					result.setMessage("操作有误");
					return result;
				}
				//上传七牛
				String qiniuUrl=new QiniuUtil().getQiNiuUrl(localFilePath);
				if(!StringUtils.hasText(qiniuUrl)) {
					result.setStatus(false);
					result.setMessage("操作有误");
					return result;
				}
				map.put("imgUrl", qiniuUrl);
				result.setData(map);
			} catch (ClientProtocolException e) {
				result.setStatus(false);
				result.setMessage("操作有误");
				e.printStackTrace();
			} catch (IOException e) {
				result.setStatus(false);
				result.setMessage("操作有误");
				e.printStackTrace();
			}
			return result;
		}
		return null;
	}

	

}
