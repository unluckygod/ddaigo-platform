/**
 * *************************************
 * Copyright(c)2016 Dingdang Yixia 
 * All Rights Reserved.
 *
 * @File: AccountHandleServiceImpl.java
 * @Package: com.purchase.service.impl
 * @Description:
 * 
 * @Version: V1.0
 * @Author: Yidong.Xiang
 * @Date: 2018年6月27日 下午2:58:28
 * 
 * *************************************
 */
package com.purchase.service.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.alibaba.fastjson.JSONObject;
import com.purchase.entity.AccountAward;
import com.purchase.entity.AccountBonus;
import com.purchase.entity.AccountIncome;
import com.purchase.entity.AppUser;
import com.purchase.entity.Member;
import com.purchase.entity.Order;
import com.purchase.entity.Vendor;
import com.purchase.entity.WxUser;
import com.purchase.mapper.AccountAwardMapper;
import com.purchase.mapper.AccountBonusMapper;
import com.purchase.mapper.AccountIncomeMapper;
import com.purchase.mapper.AppUserMapper;
import com.purchase.mapper.MemberMapper;
import com.purchase.mapper.OrderMapper;
import com.purchase.mapper.VendorMapper;
import com.purchase.mapper.WxUserMapper;
import com.purchase.service.AccountHandleService;
import com.purchase.util.DateUtil;
import com.purchase.util.LogInfo;
import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName: AccountHandleServiceImpl.java
 * @Module: 佣金业务逻辑模块
 * @Description: 
 * 
 * @author: Yidong.Xiang
 * @date: 2018年6月27日 下午2:58:28
 * 
 */
@Slf4j
@Service("accountHandleService")
public class AccountHandleServiceImpl implements AccountHandleService {
	static String incomeGrade_VL1 = "VL1";
	static String incomeGrade_PVL1 = "PVL1";
	static String incomeGrade_TEAM = "TEAM";
	static String incomeGrade_VL2 = "VL2";
	static String incomeGrade_VL3 = "VL3";

	static String incomeGrade_M2M = "VL0-VL0";//会员推荐会员奖金类型
	static String incomeGrade_V2M = "VL1-VL0";//代购推荐会员奖金类型
	static String incomeGrade_V2V = "VL1-VL1";//代购推荐代购奖金类型
	static String incomeGrade_D2M = "VL2-VL0";//总监推荐会员奖金类型
	static String incomeGrade_D2V = "VL2-VL1";//总监推荐代购奖金类型
	static String incomeGrade_GM2M = "VL3-VL0";//总经理推荐会员奖金类型
	static String incomeGrade_GM2V = "VL3-VL1";//总经理推荐代购奖金类型
	
	
	static BigDecimal discountRate = BigDecimal.valueOf(0.9);//VIP折上折比例
	static BigDecimal nvbRate = BigDecimal.valueOf(1).subtract(discountRate);//普通用户购买的代购收益比例，=1-VIP折上折比例
	static BigDecimal vbRate = nvbRate.multiply(BigDecimal.valueOf(0.25));//VIP购买的代购收益比例，=(1-VIP折上折比例)*0.25
	
	static BigDecimal pvbRate = BigDecimal.valueOf(0.25);//直属代购收益比例
	static BigDecimal tbRate = BigDecimal.valueOf(0.20);//团队非直属代购收益比例
	static BigDecimal mgrbRate = BigDecimal.valueOf(0.3);//直属经理收益比例
	static BigDecimal dirbRate = BigDecimal.valueOf(0.3);//总监收益比例
	
	static BigDecimal member2memberAward = BigDecimal.valueOf(20);//会员推荐会员奖励（小程序端会员推会员）
	static BigDecimal vendor2memberAward = BigDecimal.valueOf(28);//代购推荐会员奖励（app端代购用户）
	static BigDecimal vendor2vendorAward = BigDecimal.valueOf(100);//代购推荐代购奖励（app端代购用户）
	
	static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	@Resource
	AccountIncomeMapper accountIncomeMapper;
	@Resource
	AccountBonusMapper accountBonusMapper;
	@Resource
	AccountAwardMapper accountAwardMapper;
	@Resource
	VendorMapper vendorMapper;
	@Resource
	MemberMapper memberMapper;
	@Resource 
	AppUserMapper appUserMapper;
	@Resource 
	WxUserMapper wxUserMapper;
	@Resource
	OrderMapper orderMapper;
	@Override
	public ResponseForm getIncome(RequestForm param) {
		ResponseForm result = new ResponseForm();
		@SuppressWarnings("unchecked")
		Map<String, Object> paramMap = (Map<String, Object>) param.getData();
		String wxUid = (String) paramMap.get("wxUid");
		try {
			if (!StringUtils.isNotBlank(wxUid)) {
				result.setStatus(false);
				result.setMessage(LogInfo.PARAM_ERROR);
				log.error(LogInfo.PARAM_ERROR);
				return result;
			}
			AccountIncome wxUser = new AccountIncome();
			wxUser.setWxUid(wxUid);
			AccountIncome income = accountIncomeMapper.selectOne(wxUser);
			result.setData(income);
			result.setTotal(1);
		}
		catch(Exception e)
		{
			result.setStatus(false);
			log.error("获取佣金账户信息error,wxUid=" + wxUid, e);
		}
		return result;
	}

	@Override
	public JSONObject getIncomeDetails(String wxUid) {
		JSONObject result = new JSONObject();
		result.put("status", false);
		result.put("total", 0);
		try {
			if (!StringUtils.isNotBlank(wxUid)) {
				result.put("message", LogInfo.PARAM_ERROR);
				log.error(LogInfo.PARAM_ERROR);
				return result;
			}
			AccountIncome wxUser = new AccountIncome();
			wxUser.setWxUid(wxUid);
			AccountIncome income = accountIncomeMapper.selectOne(wxUser);
//			result.setData(income);
			result.put("total", 1);
		}
		catch(Exception e)
		{
			log.error("获取佣金账户信息error,wxUid=" + wxUid, e);
			result.put("message", LogInfo.ERROR);
			return result;
		}
		return result;
	}
	@Override
//	@Transactional(rollbackFor = Exception.class)
	public int createVendorAccount(AppUser appUser) {
		if(appUser==null)return 0;
		int r = 0;
		try {
			Date now = sdf.parse(sdf.format(new Date()));
			
			String wxUid  = appUser.getWxUid();
			String inviteCode = appUser.getInviteCode();
			String parentInviteCode = appUser.getParentInviteCode();
			boolean seedUser = inviteCode.equals(parentInviteCode) ? true:false;
			String parentWxuid = wxUid;
			if(!seedUser)
			{
				//非种子用户则通过邀请码找用户的推荐人身份信息（有可能是种子用户，种子用户是顶级）
				AppUser parentParm = new AppUser();
				parentParm.setInviteCode(appUser.getParentInviteCode());
				AppUser parentUser = appUserMapper.selectOne(parentParm);//通过邀请码查找
				parentWxuid = parentUser.getWxUid();
			}
			
			//r1.不允许重复,r2.普通用户
			Vendor arg0 = new Vendor();
			arg0.setWxUid(wxUid);
			Vendor hasVendor = vendorMapper.selectOne(arg0);
			if(hasVendor!=null)
				if(wxUid.equals(hasVendor.getWxUid()))//已存在代购了
					return 0;
			
			//创建直属代购
			Vendor vendor =  new Vendor();
			vendor.setParentWxUid(parentWxuid);
			vendor.setWxUid(wxUid);
			vendor.setInviteCode(appUser.getInviteCode());
			vendor.setStatus(appUser.getUserStatus()+"");
//			vendor.setLevel(level);
			vendor.setCreateTime(now);
			r = vendorMapper.insert(vendor);
			
			//创建代购的佣金账户
			AccountIncome accountIncome = new AccountIncome();
			accountIncome.setWxUid(appUser.getWxUid());
			accountIncome.setParentWxUid(parentWxuid);
			accountIncome.setInviteCode(appUser.getInviteCode());
			accountIncome.setWeixinId(appUser.getWeixinId());
			accountIncome.setAlipayId(appUser.getAlipayId());
			accountIncome.setMobile(appUser.getVerifyPhone());
			accountIncome.setUserStatus(appUser.getUserStatus()+"");
			accountIncome.setCreateTime(now);
			if(r>0)
				r = accountIncomeMapper.insert(accountIncome);
			
			return r;
		}
		catch(Exception e)
		{
			log.error("创建佣金账户error,wxUid=" + appUser.getWxUid(), e);
//			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		return 0;
	}
	@Override
	public int createBonus(String orderWxuid,String orderId,String tradeType) {
		int r = 0;
		if(orderWxuid==null || orderId== null || tradeType==null)return r;
		if("".equals(orderWxuid) || "".equals(orderId) || "".equals(tradeType))return r;
		
		AccountBonus vendorBonus = new AccountBonus();//代购销售佣金
		AccountBonus pvendorBonus = new AccountBonus();//直属代购奖金
		AccountBonus teamBonus = new AccountBonus();//团队非直属代购奖金
		AccountBonus mgrBonus = new AccountBonus();//直属经理管理奖金
		AccountBonus directorBonus = new AccountBonus();//总监管理奖金
		try {
			Date now = sdf.parse(sdf.format(new Date()));
			Vendor vendor = new Vendor();//订单关联的代购
			//根据下单的客户端类型找出购买用户信息
			int buyerStatus =0;//下单用户身份，默认下单用户是普通用户
			boolean seedUser = false;//种子代购标识
			boolean selfBuying = false;//代购、总监、总经理自购标识
			if("APP".equals(tradeType))//app端用户下单
			{
				AppUser arg0 = new AppUser();
				arg0.setWxUid(orderWxuid);
				AppUser buyer = appUserMapper.selectOne(arg0);
				buyerStatus = buyer.getUserStatus();
				String inviteCode = buyer.getParentInviteCode();//app端用户（普通用户或会员）绑定代购的邀请码
				//APP端用户下单无代购邀请码的不计算佣金，直接跳出
				//可能是这两种情况：1）空降代购无上级代购（不太可能）
				if(inviteCode==null || "".equals(inviteCode)) return r;
				
				//找出订单关联的代购信息
				Vendor arg1 = new Vendor();
				arg1.setWxUid(orderWxuid);
				vendor = vendorMapper.selectOne(arg1);
			}
			else//JSAPI 小程序端用户下单
			{
				WxUser arg = new WxUser();
				arg.setWxUid(orderWxuid);
				WxUser wxUser = wxUserMapper.selectOne(arg);
				buyerStatus = wxUser.getUserStatus();
				String inviteCode = wxUser.getInviteCode();//小程序端用户（普通用户或会员）绑定代购的邀请码
				//小程序端用户下单无代购邀请码的不计算佣金，直接跳出
				if(inviteCode==null || "".equals(inviteCode)) return r;
				
				//根据邀请码找出订单关联的代购信息
				Vendor arg1 = new Vendor();
				arg1.setInviteCode(inviteCode);
				vendor = vendorMapper.selectOne(arg1);

				//如果代购通过小程序自己购买自己分享的商品，算自购也无佣金。
				//通过邀请码判断是否是自购，
//				if(inviteCode.equals(vendor.getInviteCode())) //排除代购通过小程序端自购提佣
//					selfBuying =true;//代购自购如果直接退出，则导致代购的直属代购无法提佣了，因此按自购逻辑处理
				
			}
			//判断代购身份是否是种子用户
			if(vendor!=null)
			{
				if(vendor.getWxUid() !=null
					&& vendor.getParentWxUid() !=null
					&& vendor.getWxUid().equals(vendor.getParentWxUid()))
					seedUser = true;
			}
			//如果购买用户的身份是2:代购,3:总监,4:总经理其中之一，则该订单是自购
			if(buyerStatus>1 
				&& buyerStatus<10 //排除ios苹果端游客身份
				)
				selfBuying =true;

			//如果是自购，则该代购不返佣金，直属代购和总监和总经理正常享有团队奖金分成
//			if(orderWxuid.equals(vendor.getWxUid())) return r;//排除代购通过app端自购提佣
			
			//找出直属代购用户信息
//			AppUser arg2 = new AppUser();
//			arg2.setWxUid(vendor.getParentWxUid());
//			AppUser parentVendor = appUserMapper.selectOne(arg2);
			Vendor arg1 = new Vendor();
			arg1.setWxUid(vendor.getParentWxUid());
			Vendor parentVendor = vendorMapper.selectOne(arg1);
			
			//获取订单的折扣金额
//			BigDecimal discountAmount = orderMapper.getOrderDiscountAmount(orderId);
//			//订单支付金额
//			BigDecimal payAmount = BigDecimal.valueOf(Double.parseDouble(orderPrice));
			//计算订单优惠金额(节省部分的金额)
			BigDecimal rebateAmount = BigDecimal.valueOf(0);
//			if(discountAmount!=null && payAmount!=null)
//				reduceAmount = discountAmount.subtract(payAmount);
			BigDecimal reduceAmount = accountIncomeMapper.getOrderReduceAmount(orderId);//从商品中直接计算节省金额
			
			//根据购买用户身份创建佣金明细
			
			BigDecimal incomeRate = BigDecimal.valueOf(0);
			BigDecimal vendorAmount = BigDecimal.valueOf(0);//代购销售提成金额
			BigDecimal pvendorAmount = BigDecimal.valueOf(0);//直属代购奖金金额
			BigDecimal teamAmount = BigDecimal.valueOf(0);//团队非直属代购奖金金额
			BigDecimal mgrAmount = BigDecimal.valueOf(0);//直属经理奖金金额
			BigDecimal dirAmount = BigDecimal.valueOf(0);//总监奖金金额
			if(0==buyerStatus)//普通用户（非会员）购买
			{
				rebateAmount = reduceAmount;//代购提成=折扣节省金额
				incomeRate = nvbRate;//非会员（普通用户）购买的代购收益比例
			}
			else 
			{
				rebateAmount = reduceAmount.multiply(BigDecimal.valueOf(0.25));//代购提成=折扣节省金额*vbRate
				incomeRate = vbRate;//会员用户购买的代购收益比例
			}

			vendorAmount = rebateAmount;//代购提成=折扣节省金额
			pvendorAmount = rebateAmount.multiply(pvbRate);//直属代购收益金额
			teamAmount = rebateAmount.multiply(tbRate);//团队非直属代购奖金
			mgrAmount = teamAmount.multiply(mgrbRate);//直属经理奖金
			dirAmount = teamAmount.multiply(dirbRate);//总监奖金
			
			//计算代购销售佣金
			vendorBonus.setWxUid(vendor.getWxUid());
			vendorBonus.setParentWxUid(vendor.getParentWxUid());
			vendorBonus.setInviteCode(vendor.getInviteCode());
			vendorBonus.setOrderId(orderId);
			vendorBonus.setIncomeStatus("0");//默认预计收益状态，7天后无退款状态改成可提现状态1
			vendorBonus.setIncomeGrade(incomeGrade_VL1);
			vendorBonus.setIncomeRate(incomeRate);
			vendorBonus.setIncomeAmount(vendorAmount);
//			vendorBonus.setIncomeDate(incomeDate);//7天后无退款记录确认时间
			vendorBonus.setCreateTime(now);
			
			//计算直属代购奖金
			pvendorBonus.setWxUid(parentVendor.getWxUid());
			pvendorBonus.setParentWxUid(parentVendor.getParentWxUid());//找出上级代购
			pvendorBonus.setInviteCode(parentVendor.getInviteCode());//找出上级代购邀请码
			pvendorBonus.setOrderId(orderId);
			pvendorBonus.setIncomeStatus("0");//默认预计收益状态，7天后无退款状态改成可提现状态1
			pvendorBonus.setIncomeGrade(incomeGrade_PVL1);
			pvendorBonus.setIncomeRate(pvbRate);
			pvendorBonus.setIncomeAmount(pvendorAmount);
//			pvendorBonus.setIncomeDate(incomeDate);//7天后无退款记录确认时间
			pvendorBonus.setCreateTime(now);		
			
			//计算团队非直属代购奖金
			teamBonus.setWxUid(vendor.getWxUid());
			teamBonus.setParentWxUid(vendor.getParentWxUid());//找出上级代购
			teamBonus.setInviteCode(vendor.getInviteCode());//找出上级代购邀请码
			teamBonus.setOrderId(orderId);
			teamBonus.setIncomeStatus("0");//默认预计收益状态，7天后无退款状态改成可提现状态1
			teamBonus.setIncomeGrade(incomeGrade_TEAM);
			teamBonus.setIncomeRate(tbRate);
			teamBonus.setIncomeAmount(teamAmount);
//			teamBonus.setIncomeDate(incomeDate);//7天后无退款记录确认时间
			teamBonus.setCreateTime(now);
			
			//计算直属经理管理奖金（没有人就不分配）
//			mgrBonus.setWxUid(vendor.getParentWxUid());
////			mgrBonus.setParentWxUid(vendor.getParentWxUid());//找出上级代购
////			mgrBonus.setInviteCode(vendor.getInviteCode());//找出上级代购邀请码
//			mgrBonus.setOrderId(orderId);
//			mgrBonus.setIncomeStatus("0");//默认预计收益状态，7天后无退款状态改成可提现状态1
//			mgrBonus.setIncomeGrade(incomeGrade_TEAM);
//			mgrBonus.setIncomeRate(tbRate);
//			mgrBonus.setIncomeAmount(teamAmount);
////			mgrBonus.setIncomeDate(incomeDate);//7天后无退款记录确认时间
//			mgrBonus.setCreateTime(new Date());
			//计算总监管理奖金（没有人就不分配）
			
			//保存佣金明细
			if(!selfBuying)//代购A自购，则A无提佣
				accountBonusMapper.insert(vendorBonus);
			if(!vendor.getWxUid().equals(vendor.getParentWxUid()))//
				accountBonusMapper.insert(pvendorBonus);//直属代购正常提佣
			accountBonusMapper.insert(teamBonus);
			//更新佣金账户
			if(!selfBuying)//代购A自购，则A无提佣，不更新A的佣金账户
				accountIncomeMapper.updatePreIncomeAmount(vendorBonus.getWxUid(), vendorBonus.getIncomeAmount());
			if(!vendor.getWxUid().equals(vendor.getParentWxUid()))
				accountIncomeMapper.updatePreIncomeAmount(pvendorBonus.getWxUid(), pvendorBonus.getIncomeAmount());//直属代购正常提佣
		}
		catch(Exception e)
		{
			log.error("创建佣金明细error,wxUid=" + orderWxuid, e);
		}
		return r;
	}
	@Override
	public int createVendorAward(AppUser appUser,String orderId) {
		int r = 0;
		if(appUser==null)return r;
		String wxUid = appUser.getWxUid();
		String parentInviteCode = appUser.getParentInviteCode();
		if(wxUid==null)return r;
		if(parentInviteCode==null || "".equals(parentInviteCode)) return r;//自然流量的用户无推荐人，必须要邀请码
		try {
			Date now = sdf.parse(sdf.format(new Date()));
			String inviteCode = appUser.getInviteCode();
			boolean seedUser = inviteCode.equals(parentInviteCode) ? true:false;
			String parentWxUid = wxUid;
			AppUser vendor = appUser;//推荐人
			AppUser parentVendor = new AppUser();//推荐人的直属代购
			if(!seedUser)
			{
				AppUser parentParm = new AppUser();
				parentParm.setInviteCode(parentInviteCode);
				vendor = appUserMapper.selectOne(parentParm);//通过邀请码查找
				//非种子用户则通过邀请码找用户的推荐人身份信息（有可能是种子用户，种子用户是顶级）
				if(vendor.getInviteCode().equals(vendor.getParentInviteCode()))
					parentWxUid = vendor.getWxUid();
				else {
					AppUser arg1 = new AppUser();
					arg1.setInviteCode(vendor.getParentInviteCode());
					parentVendor = appUserMapper.selectOne(arg1);
					parentWxUid = parentVendor.getWxUid();
				}
			}
			
			//判断推荐人和购买用户的身份
			int userStatus =  vendor.getUserStatus();//代购身份（推荐人）
			BigDecimal incomeAmount = BigDecimal.valueOf(0);
			String incomeGrade = "";
			if(2==userStatus)//代购推荐普通用户购买399成为代购
			{
				incomeGrade = incomeGrade_V2V;
				incomeAmount = vendor2vendorAward;
				//创建代购推荐奖励佣金
				AccountAward award = new AccountAward();//会员的奖金
				award.setWxUid(vendor.getWxUid());
				award.setParentWxUid(parentWxUid);
				award.setOrderId(orderId);
				award.setIncomeGrade(incomeGrade);
				award.setIncomeAmount(incomeAmount);
				award.setIncomeDate(now);
				accountAwardMapper.insert(award);
				
				//更新推荐人账号收益
				accountIncomeMapper.updateMemberPreIncomeAmount(vendor.getWxUid(), vendor2vendorAward);
			}
		}
		catch(Exception e)
		{
			log.error("创建佣金账户error,wxUid=" + wxUid, e);
		}
		return r;
	}
	@Override
	public int createMemberAccount(String wxUid,String parentWxUid,String orderId) {
		if(wxUid==null||orderId==null)return 0;
		int r = 0;
		try {
			
			
//			String wxUid  = wxUser.getWxUid();
//			String inviteCode = wxUser.getInviteCode();
//			String parentWxuid = wxUser.getParentWxuid();
//			boolean seedUser = wxUid.equals(parentWxuid) ? true:false;
//			if(!seedUser)
//			{
//				//非种子用户则通过邀请码找用户的推荐人身份信息（有可能是种子用户，种子用户是顶级）
//				AppUser parentParm = new AppUser();
//				parentParm.setInviteCode(appUser.getParentInviteCode());
//				AppUser parentUser = appUserMapper.selectOne(parentParm);//通过邀请码查找
//				parentWxuid = parentUser.getWxUid();
//			}
			
			//r1.不允许重复,r2.普通用户
			Member arg0 = new Member();
			arg0.setWxUid(wxUid);
			Member hasPerson = memberMapper.selectOne(arg0);
			if(hasPerson!=null)
				if(wxUid.equals(hasPerson.getWxUid()))//会员已存在
					return 0;
			
			Date now = sdf.parse(sdf.format(new Date()));
			//创建会员关系
			Member member =  new Member();
			member.setParentWxUid(parentWxUid);
			member.setWxUid(wxUid);
			member.setStatus(1);
			member.setCreateTime(now);
			r = memberMapper.insert(member);
			
			//创建会员的佣金账户
			AccountIncome accountIncome = new AccountIncome();
			accountIncome.setWxUid(wxUid);
			accountIncome.setParentWxUid(parentWxUid);
//			accountIncome.setInviteCode(appUser.getInviteCode());
//			accountIncome.setWeixinId(appUser.getWeixinId());
//			accountIncome.setAlipayId(appUser.getAlipayId());
//			accountIncome.setMobile(appUser.getVerifyPhone());
			accountIncome.setUserStatus("1");
			accountIncome.setCreateTime(now);
			if(r>0)
				r = accountIncomeMapper.insert(accountIncome);
			
			return r;
		}
		catch(Exception e)
		{
			log.error("创建会员佣金账户error,wxUid=" + wxUid, e);
		}
		return 0;
	}
	@Override
	public int createMemberAward(String wxUid, String parentWxUid, String orderId) {
		int r = 0;
		if(wxUid==null && parentWxUid==null) return r;
		if(wxUid!=null && (parentWxUid==null || "".equals(parentWxUid))) return r;//自然流量用户（无推荐人）购买88会员无奖励
		if(wxUid.equals(parentWxUid)) return r;//自己购买88无奖励
		try {
			//创建会员推荐奖励佣金
			Date now = sdf.parse(sdf.format(new Date()));
			AccountAward award = new AccountAward();//会员的奖金
			award.setWxUid(wxUid);
			award.setParentWxUid(parentWxUid);
			award.setOrderId(orderId);
			award.setIncomeGrade(incomeGrade_M2M);
			award.setIncomeAmount(member2memberAward);
			award.setIncomeDate(now);
			accountAwardMapper.insert(award);
			
			//更新推荐人账号收益
			accountIncomeMapper.updateMemberPreIncomeAmount(parentWxUid, member2memberAward);
		}
		catch(Exception e)
		{
			log.error("创建会员佣金明细error,wxUid=" + wxUid, e);
		}
		return 0;
	}
}
