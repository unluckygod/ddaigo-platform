package com.purchase.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.purchase.entity.Goods;
import com.purchase.entity.GoodsCategory;
import com.purchase.mapper.GoodsCategoryMapper;
import com.purchase.mapper.GoodsMapper;
import com.purchase.service.GoodsCategoryService;
import com.purchase.util.LogInfo;
import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;


@Slf4j
@Service
public class GoodsCategoryServiceImpl implements GoodsCategoryService {

	@Resource
	GoodsCategoryMapper gcMapper;
	
	@Resource
	GoodsMapper goodsMapper;
	
	@Override
	public ResponseForm getGoodsCategoryByPid(RequestForm param) {
		ResponseForm result = new ResponseForm();
		Map<String, Object> paramMap = (Map<String, Object>) param.getData();
		try {
//			GoodsCategory gcBean = new GoodsCategory();
//			MyBeanUtils.populate(user, paramMap);
			String pid = (String)paramMap.get("categoryPid");
			if(!StringUtils.isNotBlank(pid)){
				result.setStatus(false);
				result.setMessage(LogInfo.PARAM_ERROR);
				log.error(LogInfo.PARAM_ERROR);
				return result;
			}
			
			Example example = new Example(GoodsCategory.class);
			example.setOrderByClause("category_sort DESC ");
			Criteria criteria=example.createCriteria();
			criteria.andEqualTo("categoryPid", pid);
			criteria.andEqualTo("categoryStatus", 0);
			List<GoodsCategory> list = gcMapper.selectByExample(example);
			result.setData(list);
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error(LogInfo.ERROR);
		}
		return result;
	}

	@Override
	public ResponseForm getGoodsCategoryCount(RequestForm param) {
		ResponseForm result = new ResponseForm();
		Map<String, Object> paramMap = (Map<String, Object>) param.getData();
		try {
//			GoodsCategory gcBean = new GoodsCategory();
//			MyBeanUtils.populate(user, paramMap);
			String categoryId = (String)paramMap.get("categoryId");
			if(!StringUtils.isNotBlank(categoryId)){
				result.setStatus(false);
				result.setMessage(LogInfo.PARAM_ERROR);
				log.error(LogInfo.PARAM_ERROR);
				return result;
			}
			
			Example example = new Example(Goods.class);
			example.setCountProperty("goods_id");
			Criteria criteria =example.createCriteria();
			criteria.andEqualTo("categoryId", categoryId);
			int count = goodsMapper.selectCountByExample(example);
			result.setData(count);
			
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error(LogInfo.ERROR);
		}
		return result;
	}

}
