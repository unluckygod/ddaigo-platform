package com.purchase.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.purchase.entity.ActivityImage;
import com.purchase.entity.ActivityWithCoupon;
import com.purchase.entity.Banner;
import com.purchase.entity.Coupon;
import com.purchase.entity.GoodsImage;
import com.purchase.entity.GoodsStock;
import com.purchase.entity.IndexBean;
import com.purchase.entity.Store;
import com.purchase.entity.StoreActivity;
import com.purchase.mapper.ActivityImageMapper;
import com.purchase.mapper.BannerMapper;
import com.purchase.mapper.CouponMapper;
import com.purchase.mapper.GoodsImageMapper;
import com.purchase.mapper.GoodsMapper;
import com.purchase.mapper.GoodsStockMapper;
import com.purchase.mapper.StoreActivityMapper;
import com.purchase.mapper.StoreMapper;
import com.purchase.mapper.WxUserMapper;
import com.purchase.service.StoreActivityService;
import com.purchase.util.AuthUtil;
import com.purchase.util.LogInfo;
import com.purchase.util.MyBeanUtils;
import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Slf4j
@Service
public class StoreActivityServiceImpl implements StoreActivityService {
	
	@Resource
	StoreActivityMapper storeActivityMapper;
	
	@Resource
	GoodsMapper goodsMapper;
	
	
	@Resource
	GoodsImageMapper goodsImageMapper;
	
	@Resource
	GoodsStockMapper goodsStockMapper;
	
	@Resource
	BannerMapper bannerMapper;
	
	@Resource
	ActivityImageMapper activityImageMapper;
	
	@Resource
	CouponMapper couponMapper;
	
	@Resource
	StoreMapper  storeMapper;
	
	@Autowired
	WxUserMapper wxUserMapper;
	
	@Override
	public ResponseForm activityList(RequestForm param,HttpServletRequest request) {
		ResponseForm result = new ResponseForm();
		Map<String, Object> paramMap = (Map<String, Object>) param.getData();
		try {
			Long userId;
//			用户认证
			try {
				userId = AuthUtil.getUserId(request);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				result.setStatus(false);
				result.setCode("500");
				result.setMessage(LogInfo.AUTH_ERROR);
				log.error(LogInfo.AUTH_ERROR);
				return result;
			}
//			System.out.println("wxUid===>>>"+wxUid);
			String storeNo =(String) paramMap.get("storeNo");
			if(!StringUtils.isNotBlank(storeNo)){
				result.setStatus(false);
				result.setMessage(LogInfo.PARAM_ERROR);
				log.error(LogInfo.PARAM_ERROR);
				return result;
			}
			Example example = new Example(StoreActivity.class);
			example.setOrderByClause("sort DESC,create_time DESC");
			Criteria criteria=example.createCriteria();
			criteria.andNotEqualTo("status", 2);
			List<StoreActivity> saList= storeActivityMapper.selectByExample(example);
			result.setData(saList);
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error(LogInfo.ERROR);
		}
		return result;
	}

	@Override
	public ResponseForm getActivityDetail(RequestForm param, HttpServletRequest request) {
		ResponseForm result = new ResponseForm();
		Map<String, Object> paramMap = (Map<String, Object>) param.getData();
		try {
			
			Long userId;
//			用户认证
			try {
				userId = AuthUtil.getUserId(request);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				result.setStatus(false);
				result.setCode("500");
				result.setMessage(LogInfo.AUTH_ERROR);
				log.error(LogInfo.AUTH_ERROR);
				return result;
			}
//			System.out.println("wxUid===>>>"+wxUid);
//			String storeNo =(String) paramMap.get("storeNo");
			String id =(String) paramMap.get("id");
			if(!StringUtils.isNotBlank(id)){
				result.setStatus(false);
				result.setMessage(LogInfo.PARAM_ERROR);
				log.error(LogInfo.PARAM_ERROR);
				return result;
			}
			StoreActivity storeActivity= storeActivityMapper.selectByPrimaryKey(Integer.valueOf(id));
			
			//查询活动图片
			Example example = new Example(ActivityImage.class);
			Criteria criteria=example.createCriteria();
			criteria.andEqualTo("activityId", storeActivity.getId());
			List<ActivityImage> imageList = activityImageMapper.selectByExample(example);
			storeActivity.setImageList(imageList);
			
//			//关联店铺
//			paramMap.put("activityId", storeActivity.getId());
//			List<Store> storeList = storeMapper.getActivityStore(paramMap);
//			storeActivity.setStoreList(storeList);
			
			result.setData(storeActivity);
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error(LogInfo.ERROR);
		}
		return result;
	}
	@Transactional(rollbackFor=Exception.class)
	@Override
	public ResponseForm addActivity(RequestForm param, HttpServletRequest request) {
		ResponseForm result = new ResponseForm();
		Map<String, Object> paramMap = (Map<String, Object>) param.getData();
		try {
			Long userId;
//			用户认证
			try {
				userId = AuthUtil.getUserId(request);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				result.setStatus(false);
				result.setCode("500");
				result.setMessage(LogInfo.AUTH_ERROR);
				log.error(LogInfo.AUTH_ERROR);
				return result;
			}
			
			
			//String storeNo =(String) paramMap.get("storeNo");
			List imageList = (List) paramMap.get("imageList");
//			List storeList = (List) paramMap.get("storeList");
//			if(!StringUtils.isNotBlank(storeNo)){
//				result.setStatus(false);
//				result.setMessage(LogInfo.PARAM_ERROR);
//				log.error(LogInfo.PARAM_ERROR);
//				return result;
//			}
			StoreActivity storeActivity = new StoreActivity();
			MyBeanUtils.populate(storeActivity, paramMap);
			storeActivity.setCreateTime(new Date());
			storeActivity.setType(1);
			storeActivityMapper.insertSelective(storeActivity);
			//处理活动介绍图片
			if(null!=imageList&&imageList.size()>0){
				for (int i = 0; i < imageList.size(); i++) {
					ActivityImage activityImage = new ActivityImage();
					activityImage.setMediaType(1);
					activityImage.setActivityId(storeActivity.getId());
					activityImage.setCreateTime(new Date());
					activityImage.setMediaUrl(String.valueOf(imageList.get(i)));
					activityImageMapper .insertSelective(activityImage);
					
				}
				
			}
//			//活动关联店铺
//			if(null!=storeList&&storeList.size()>0){
//				for (int i = 0; i < storeList.size(); i++) {
//					StoreActivityRelation storeActivityRelation = new StoreActivityRelation();
//					storeActivityRelation.setStoreNo(String.valueOf(storeList.get(i)));
//					storeActivityRelation.setActivityId(storeActivity.getId());
//					storeActivityRelationMapper .insertSelective(storeActivityRelation);
//				}
//				
//			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error(LogInfo.ERROR);
		}
		return result;
	}
	@Transactional(rollbackFor=Exception.class)
	@Override
	public ResponseForm editActivity(RequestForm param, HttpServletRequest request) {
		ResponseForm result = new ResponseForm();
		Map<String, Object> paramMap = (Map<String, Object>) param.getData();
		try {
			Long userId;
//			用户认证
			try {
				userId = AuthUtil.getUserId(request);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				result.setStatus(false);
				result.setCode("500");
				result.setMessage(LogInfo.AUTH_ERROR);
				log.error(LogInfo.AUTH_ERROR);
				return result;
			}
			//String storeNo =(String) paramMap.get("storeNo");
			List imageList = (List) paramMap.get("imageList");
//			List storeList = (List) paramMap.get("storeList");
//			if(!StringUtils.isNotBlank(storeNo)){
//				result.setStatus(false);
//				result.setMessage(LogInfo.PARAM_ERROR);
//				log.error(LogInfo.PARAM_ERROR);
//				return result;
//			}
			
			StoreActivity storeActivity = new StoreActivity();
			MyBeanUtils.populate(storeActivity, paramMap);
			
			storeActivityMapper.updateByPrimaryKeySelective(storeActivity);
			//处理活动介绍图片
			if(null!=imageList&&imageList.size()>0){
				ActivityImage activityImage;
				activityImage = new ActivityImage();
				activityImage.setActivityId(storeActivity.getId());
				activityImageMapper.delete(activityImage);
				for (int i = 0; i < imageList.size(); i++) {
					activityImage = new ActivityImage();
					activityImage.setMediaType(1);
					activityImage.setActivityId(storeActivity.getId());
					activityImage.setCreateTime(new Date());
					activityImage.setMediaUrl(String.valueOf(imageList.get(i)));
					activityImageMapper .insertSelective(activityImage);
				}
			}
//			//处理关联店铺
//			if(null!=storeList&&storeList.size()>0){
//				StoreActivityRelation storeActivityRelation;
//				storeActivityRelation = new StoreActivityRelation();
//				storeActivityRelation.setActivityId(storeActivity.getId());
//				storeActivityRelationMapper.delete(storeActivityRelation);
//				for (int i = 0; i < storeList.size(); i++) {
//					storeActivityRelation = new StoreActivityRelation();
//					storeActivityRelation.setStoreNo(String.valueOf(storeList.get(i)));;
//					storeActivityRelation.setActivityId(storeActivity.getId());
//					storeActivityRelationMapper .insertSelective(storeActivityRelation);
//				}
//			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error(LogInfo.ERROR);
		}
		return result;
	}

	@SuppressWarnings({ "unused", "unchecked" })
	@Override
	public ResponseForm getActivityGoodsList(RequestForm param, HttpServletRequest request) {
		ResponseForm result = new ResponseForm();
		Map<String, Object> paramMap = (Map<String, Object>) param.getData();
		try {
			String wxUid = (String) paramMap.get("wxUid");
//			Integer userStatus = (Integer) paramMap.get("userStatus");
//			MemberBenefit memBenefit = memBenefitMapper.selectByPrimaryKey(userStatus);
//			String memDiscount = memBenefit.getMemDiscount();
			Long userId;
//			用户认证
			try {
				userId = AuthUtil.getUserId(request);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				result.setStatus(false);
				result.setCode("500");
				result.setMessage(LogInfo.AUTH_ERROR);
				log.error(LogInfo.AUTH_ERROR);
				return result;
			}
			String storeNo =(String) paramMap.get("storeNo");
			if(!StringUtils.isNotBlank(storeNo)){
				result.setStatus(false);
				result.setMessage(LogInfo.PARAM_ERROR);
				log.error(LogInfo.PARAM_ERROR);
				return result;
			}
			com.purchase.util.Page.parsePage(paramMap);
			Page<Object> pageHelper = PageHelper.startPage((int) paramMap.get("page"), (int) paramMap.get("size"));
			List<Map<String, Object>> list=storeActivityMapper.getActivityGoods(paramMap);
			for (Map<String, Object> map : list) {
				Integer goodsId = (Integer) map.get("goods_id");
				//关联商品图片
				GoodsImage goodsImage = new GoodsImage();
				goodsImage.setGoodsId(goodsId);
				List<GoodsImage> gmList = goodsImageMapper.select(goodsImage); 
				map.put("goodsImages", gmList);
				
				GoodsStock goodsStock = new GoodsStock();
				goodsStock.setGoodsId(goodsId);
				List<GoodsStock> gsList = goodsStockMapper.select(goodsStock); 
				map.put("goodsSize", gsList);
				
				//会员权益和价格
//				if(StringUtils.isNotBlank(memDiscount)) {
//					map.put("memDiscount", memDiscount);
//					if(map.get("discount_price")!=null) {
//						map.put("memPrice", memPrice(memDiscount, map.get("discount_price")));
//					}else{
//						map.put("memPrice",memPrice(memDiscount, map.get("goods_price")));
//					}
//				}
				
				//商品颜色
				List<Map<String, Object>> goodsColour =null ;
				String relation = (String) map.get("relation");
				if(relation!=null &&!"".equals(relation)){
					Map<String,Object> relationMap = new HashMap<String, Object>();
					relationMap.put("storeNo", storeNo);
					relationMap.put("relation", relation);
					goodsColour =goodsMapper.getGoodsColourList(relationMap);
				}else{
					Map<String, Object> gMap = new HashMap<String, Object>();
					gMap.put("goods_name", map.get("goods_name"));
					gMap.put("goods_code", map.get("goods_code"));
					gMap.put("goods_color", map.get("goods_color"));
					gMap.put("goods_id", map.get("goods_id"));
					gMap.put("soldout", map.get("soldout"));
					goodsColour = new ArrayList<Map<String, Object>>();
					goodsColour.add(gMap);
				}
				map.put("goodsColour", goodsColour);
				
			}
			
			result.setData(list);
			result.setTotal((int) pageHelper.getTotal());
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error(LogInfo.ERROR);
		}
		return result;
	}
	
	
//	/**
//	 * @desc 会员折扣价计算
//	 * @param memDiscount
//	 * @param goodsPrice
//	 * @return
//	 */
//	@SuppressWarnings("unused")
//	private BigDecimal memPrice(Object memDiscount,Object goodsPrice) {
//		BigDecimal rate = new BigDecimal(memDiscount.toString()).divide(new BigDecimal("10"));
//		BigDecimal price = new BigDecimal(goodsPrice.toString());
//		return price.multiply(rate).setScale(2, BigDecimal.ROUND_HALF_UP);
//	}
	
	@Override
	public ResponseForm searchActivityGoodsList(RequestForm param, HttpServletRequest request) {
		ResponseForm result = new ResponseForm();
		Map<String, Object> paramMap = (Map<String, Object>) param.getData();
		try {
			
			Long userId;
//			用户认证
			try {
				userId = AuthUtil.getUserId(request);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				result.setStatus(false);
				result.setCode("500");
				result.setMessage(LogInfo.AUTH_ERROR);
				log.error(LogInfo.AUTH_ERROR);
				return result;
			}
			String storeNo =(String) paramMap.get("storeNo");
			if(!StringUtils.isNotBlank(storeNo)){
				result.setStatus(false);
				result.setMessage(LogInfo.PARAM_ERROR);
				log.error(LogInfo.PARAM_ERROR);
				return result;
			}
			List<Map<String, Object>> list=storeActivityMapper.searchActivityGoods(paramMap);
			for (Map<String, Object> map : list) {
				Integer goodsId = (Integer) map.get("goods_id");
				//关联商品图片
				GoodsImage goodsImage = new GoodsImage();
				goodsImage.setGoodsId(goodsId);
				List<GoodsImage> gmList = goodsImageMapper.select(goodsImage); 
				map.put("goodsImages", gmList);
				
				GoodsStock goodsStock = new GoodsStock();
				goodsStock.setGoodsId(goodsId);
				List<GoodsStock> gsList = goodsStockMapper.select(goodsStock); 
				map.put("goodsSize", gsList);
				
				//商品颜色
				List<Map<String, Object>> goodsColour =null ;
				String relation = (String) map.get("relation");
				if(relation!=null &&!"".equals(relation)){
					Map<String,Object> relationMap = new HashMap<String, Object>();
					relationMap.put("storeNo", storeNo);
					relationMap.put("relation", relation);
					goodsColour =goodsMapper.getGoodsColourList(relationMap);
				}else{
					Map<String, Object> gMap = new HashMap<String, Object>();
					gMap.put("goods_name", map.get("goods_name"));
					gMap.put("goods_code", map.get("goods_code"));
					gMap.put("goods_color", map.get("goods_color"));
					gMap.put("goods_id", map.get("goods_id"));
					gMap.put("soldout", map.get("soldout"));
					goodsColour = new ArrayList<Map<String, Object>>();
					goodsColour.add(gMap);
				}
				map.put("goodsColour", goodsColour);
			}
			
			result.setData(list);
			
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error(LogInfo.ERROR);
		}
		return result;
	}

	@Override
	public ResponseForm getactivityListAdmin(RequestForm param, HttpServletRequest request) {
		ResponseForm result = new ResponseForm();
		Map<String, Object> paramMap = (Map<String, Object>) param.getData();
		try {
			Long userId;
//			用户认证
			try {
				userId = AuthUtil.getUserId(request);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				result.setStatus(false);
				result.setCode("500");
				result.setMessage(LogInfo.AUTH_ERROR);
				log.error(LogInfo.AUTH_ERROR);
				return result;
			}
			String name = (String) paramMap.get("name");
			String storeNo =(String) paramMap.get("storeNo");
			String status =(String) paramMap.get("status");
			if(!StringUtils.isNotBlank(storeNo)){
				result.setStatus(false);
				result.setMessage(LogInfo.PARAM_ERROR);
				log.error(LogInfo.PARAM_ERROR);
				return result;
			}
			Example example = new Example(StoreActivity.class);
			example.setOrderByClause("sort DESC,create_time DESC");
			Criteria criteria=example.createCriteria();
			if(!"".equals(status)){
				criteria.andEqualTo("status", status);
			}
			if(StringUtils.isNotBlank(name)) {
				criteria.andLike("name", "%"+name+"%");
			}
			com.purchase.util.Page.parsePage(paramMap);
			Page<Object> pageHelper = PageHelper.startPage((int) paramMap.get("page"), (int) paramMap.get("size"));
			List<StoreActivity> saList= storeActivityMapper.selectByExample(example);
			result.setData(saList);
			result.setTotal((int) pageHelper.getTotal());
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error(LogInfo.ERROR);
		}
		return result;
	}

	@Override
	public ResponseForm getAllActivityListAdmin(RequestForm param, HttpServletRequest request) {
		ResponseForm result = new ResponseForm();
		Map<String, Object> paramMap = (Map<String, Object>) param.getData();
		try {
			Long userId;
//			用户认证
			try {
				userId = AuthUtil.getUserId(request);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				result.setStatus(false);
				result.setCode("500");
				result.setMessage(LogInfo.AUTH_ERROR);
				log.error(LogInfo.AUTH_ERROR);
				return result;
			}
			
			Example example = new Example(StoreActivity.class);
			example.setOrderByClause("sort DESC,create_time DESC");
			List<StoreActivity> saList= storeActivityMapper.selectByExample(example);
			result.setData(saList);
			
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error(LogInfo.ERROR);
		}
		return result;
	}

	@Override
	public ResponseForm activityIndex(RequestForm param, HttpServletRequest request) {
		ResponseForm result = new ResponseForm();
		Map<String, Object> paramMap = (Map<String, Object>) param.getData();
		try {
			/*Long userId;
//			用户认证
			try {
				userId = AuthUtil.getUserId(request);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				result.setStatus(false);
				result.setCode("500");
				result.setMessage(LogInfo.AUTH_ERROR);
				log.error(LogInfo.AUTH_ERROR);
				return result;
			}*/
			String storeNo =(String) paramMap.get("storeNo");
			if(!StringUtils.isNotBlank(storeNo)){
				result.setStatus(false);
				result.setMessage(LogInfo.PARAM_ERROR);
				log.error(LogInfo.PARAM_ERROR);
				return result;
			}
			IndexBean indexBean = new IndexBean();
			//banner
			String platForm = (String) paramMap.get("tradeType");
			Example example = new Example(Banner.class);
			Criteria criteria=example.createCriteria();
			criteria.andEqualTo("status", 1);
//			criteria.andEqualTo("platform",platForm);
//			criteria.orEqualTo("platform","ALL");
			criteria.andNotEqualTo("platform", "JSAPI".equals(platForm)?"APP":"JSAPI");
			example.setOrderByClause("sort DESC,create_time DESC");
			List<Banner> bannerList=bannerMapper.selectByExample(example);
			indexBean.setBanner(bannerList);
			//售卖中
			List<StoreActivity> sellList= getStoreActivitys(1,storeNo);
			//展示中
			List<StoreActivity> exhibitionList= getStoreActivitys(0,storeNo);
			
			indexBean.setSellList(sellList);
			indexBean.setExhibitionList(exhibitionList);
			
			//优惠券
			Map coupon = couponMapper.getCouponByIdWithCondition(23);
			indexBean.setOcupon(coupon);
			
			result.setData(indexBean);
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error(LogInfo.ERROR);
		}
		return result;
	}
	
	
	private List<StoreActivity> getStoreActivitys(int status,String storeNo){
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("storeNo", storeNo);
		paramMap.put("status", status);
		List<StoreActivity> list= storeActivityMapper.activityIndex(paramMap);
		if(list.size()>0){
			for (int i = 0; i < list.size(); i++) {
				StoreActivity activity = list.get(i);
				Example example1 = new Example(ActivityImage.class);
				Criteria criteria1=example1.createCriteria();
				criteria1.andEqualTo("activityId", activity.getId());
				List<ActivityImage> imageList = activityImageMapper.selectByExample(example1);
				activity.setImageList(imageList);
			}
		}
		
		return list;
	}

	@Override
	public ResponseForm getActivityGoodsListWithCoupon(RequestForm param, HttpServletRequest request) {
		ResponseForm result = new ResponseForm();
		Map<String, Object> paramMap = (Map<String, Object>) param.getData();
		try {
			Long userId;
//			用户认证
			try {
				userId = AuthUtil.getUserId(request);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				result.setStatus(false);
				result.setCode("500");
				result.setMessage(LogInfo.AUTH_ERROR);
				log.error(LogInfo.AUTH_ERROR);
				return result;
			}
			String storeNo =(String) paramMap.get("storeNo");
			String activityId =(String) paramMap.get("activityId"); 
			if(!StringUtils.isNotBlank(storeNo)||!StringUtils.isNotBlank(activityId)){
				result.setStatus(false);
				result.setMessage(LogInfo.PARAM_ERROR);
				log.error(LogInfo.PARAM_ERROR);
				return result;
			}
			
			ActivityWithCoupon awc = new ActivityWithCoupon();
			//查询优惠券
			List<Coupon> cList=couponMapper.getReleaseList(paramMap);
			if(cList.size()>0){
				awc.setCouponFlag(1);
			}
			//查询商品
//			com.purchase.util.Page.parsePage(paramMap);
//			Page<Object> pageHelper = PageHelper.startPage((int) paramMap.get("page"), (int) paramMap.get("size"));
			List<Map<String, Object>> list=storeActivityMapper.getActivityGoods(paramMap);
			for (Map<String, Object> map : list) {
				Integer goodsId = (Integer) map.get("goods_id");
				//关联商品图片
				GoodsImage goodsImage = new GoodsImage();
				goodsImage.setGoodsId(goodsId);
				List<GoodsImage> gmList = goodsImageMapper.select(goodsImage); 
				map.put("goodsImages", gmList);
				
				GoodsStock goodsStock = new GoodsStock();
				goodsStock.setGoodsId(goodsId);
				List<GoodsStock> gsList = goodsStockMapper.select(goodsStock); 
				map.put("goodsSize", gsList);
				
				//商品颜色
				List<Map<String, Object>> goodsColour =null ;
				String relation = (String) map.get("relation");
				if(relation!=null &&!"".equals(relation)){
					Map<String,Object> relationMap = new HashMap<String, Object>();
					relationMap.put("storeNo", storeNo);
					relationMap.put("relation", relation);
					goodsColour =goodsMapper.getGoodsColourList(relationMap);
				}else{
					Map<String, Object> gMap = new HashMap<String, Object>();
					gMap.put("goods_name", map.get("goods_name"));
					gMap.put("goods_code", map.get("goods_code"));
					gMap.put("goods_color", map.get("goods_color"));
					gMap.put("goods_id", map.get("goods_id"));
					gMap.put("soldout", map.get("soldout"));
					goodsColour = new ArrayList<Map<String, Object>>();
					goodsColour.add(gMap);
				}
				map.put("goodsColour", goodsColour);
			}
			awc.setGoodsList(list);
			result.setData(awc);
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error(LogInfo.ERROR);
		}
		return result;
	}

	@Override
	public ResponseForm getActivityCoupon(RequestForm param, HttpServletRequest request) {
		ResponseForm result = new ResponseForm();
		Map<String, Object> paramMap = (Map<String, Object>) param.getData();
		try {
			String storeNo =(String) paramMap.get("storeNo");
			String activityId =(String) paramMap.get("activityId"); 
			if(!StringUtils.isNotBlank(storeNo)||!StringUtils.isNotBlank(activityId)){
				result.setStatus(false);
				result.setMessage(LogInfo.PARAM_ERROR);
				log.error(LogInfo.PARAM_ERROR);
				return result;
			}
			
			ActivityWithCoupon awc = new ActivityWithCoupon();
			//查询优惠券
			List<Coupon> cList=couponMapper.getReleaseList(paramMap);
			if(cList.size()>0){
				awc.setCouponFlag(1);
			}
			result.setData(awc);
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error(LogInfo.ERROR);
		}
		return result;
	}
	
	
}
