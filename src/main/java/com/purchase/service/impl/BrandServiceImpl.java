package com.purchase.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.purchase.entity.Brand;
import com.purchase.mapper.BrandMapper;
import com.purchase.service.BrandService;
import com.purchase.util.AuthUtil;
import com.purchase.util.LogInfo;
import com.purchase.util.MyBeanUtils;
import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Slf4j
@Service
public class BrandServiceImpl implements BrandService {

	@Resource
	BrandMapper brandMapper;


	@Override
	public ResponseForm getBrandList(RequestForm param, HttpServletRequest request) {
		ResponseForm result = new ResponseForm();
		Map<String, Object> paramMap = (Map<String, Object>) param.getData();
		try {
			
			// 用户认证
			try {
				AuthUtil.getUserId(request);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				result.setStatus(false);
				result.setCode("500");
				result.setMessage(LogInfo.AUTH_ERROR);
				log.error(LogInfo.AUTH_ERROR);
				return result;
			}
			com.purchase.util.Page.parsePage(paramMap);
			
			Example example = new Example(Brand.class);
			String name = (String) paramMap.get("name");
			example.createCriteria().andLike("name", "%"+(StringUtils.isBlank(name)?"":name)+"%");
//			List<Brand> list = brandMapper.selectAll();
			Page<Object> pageHelper = PageHelper.startPage((int) paramMap.get("page"), (int) paramMap.get("size"));
			List<Brand> list = brandMapper.selectByExample(example);
			result.setTotal((int) pageHelper.getTotal());
			result.setData(list);
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error(LogInfo.ERROR);
		}
		return result;
	}


	@Override
	public ResponseForm addBrand(RequestForm param, HttpServletRequest request) {
		ResponseForm result = new ResponseForm();
		Map<String, Object> paramMap = (Map<String, Object>) param.getData();
		try {
			// 用户认证
			try {
				AuthUtil.getUserId(request);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				result.setStatus(false);
				result.setCode("500");
				result.setMessage(LogInfo.AUTH_ERROR);
				log.error(LogInfo.AUTH_ERROR);
				return result;
			}
			Brand brand = new Brand();
			MyBeanUtils.populate(brand, paramMap);
			brand.setCreateTime(new Date());
			brandMapper.insertSelective(brand);
			
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error(LogInfo.ERROR);
		}
		return result;
	}


	@Override
	public ResponseForm editBrand(RequestForm param, HttpServletRequest request) {
		ResponseForm result = new ResponseForm();
		Map<String, Object> paramMap = (Map<String, Object>) param.getData();
		try {
			// 用户认证
			try {
				AuthUtil.getUserId(request);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				result.setStatus(false);
				result.setCode("500");
				result.setMessage(LogInfo.AUTH_ERROR);
				log.error(LogInfo.AUTH_ERROR);
				return result;
			}
			String brandId =(String) paramMap.get("brandId");
			if(!StringUtils.isNotBlank(brandId)){
				result.setStatus(false);
				result.setMessage(LogInfo.PARAM_ERROR);
				log.error(LogInfo.PARAM_ERROR);
				return result;
			}
			Brand brand = new Brand();
			MyBeanUtils.populate(brand, paramMap);
			brand.setId(Integer.valueOf(brandId));
			brandMapper.updateByPrimaryKey(brand);
			
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error(LogInfo.ERROR);
		}
		return result;
	}


	@Override
	public ResponseForm getBrandById(RequestForm param, HttpServletRequest request) {
		ResponseForm result = new ResponseForm();
		Map<String, Object> paramMap = (Map<String, Object>) param.getData();
		try {
			// 用户认证
			try {
				AuthUtil.getUserId(request);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				result.setStatus(false);
				result.setCode("500");
				result.setMessage(LogInfo.AUTH_ERROR);
				log.error(LogInfo.AUTH_ERROR);
				return result;
			}
			
			String brandId =(String) paramMap.get("brandId");
			if(!StringUtils.isNotBlank(brandId)){
				result.setStatus(false);
				result.setMessage(LogInfo.PARAM_ERROR);
				log.error(LogInfo.PARAM_ERROR);
				return result;
			}
			
			Brand brand = new Brand();
			MyBeanUtils.populate(brand, paramMap);
			brand=brandMapper.selectByPrimaryKey(Integer.valueOf(brandId));
			result.setData(brand);
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error(LogInfo.ERROR);
		}
		return result;
	}
}
