package com.purchase.service.impl;

import java.util.Map;

import javax.annotation.Resource;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.springframework.stereotype.Service;

import com.purchase.mapper.GoodsMapper;
import com.purchase.util.EventTag;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName: MQUtilServiceImpl.java
 * @Description: mq同步队列的消费者,订阅消息,按操作类型进行数据同步操作
 * @author: LHj
 * @date: 2017年12月27日
 */

@Slf4j
@Service
public class MQEventListener implements MessageListener {
	
	@Resource
	GoodsMapper goodsMapper;
	
	@SuppressWarnings("unchecked")
	@Override
	public void onMessage(Message msg) {
		if (msg != null) {
			if (msg instanceof ObjectMessage) {
				ObjectMessage obMsg = (ObjectMessage) msg;
				try {
					Map<String, Object> map = (Map<String, Object>) obMsg.getObject();
				
					//商品信息发生变化 调用
					if(map.containsKey("actionType")&&map.get("actionType").equals(EventTag.GOODS_CHANGE)) {
						goodsMapper.conditionCount(map);
					}
				} catch (JMSException e) {
					e.printStackTrace();
					System.out.println("[错误日志]-............"+EventTag.GOODS_CHANGE);
				}

			}
		}

	}

}
