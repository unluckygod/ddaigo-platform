package com.purchase.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanMap;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.loader.custom.Return;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.purchase.entity.AppUser;
import com.purchase.entity.Invite;
import com.purchase.entity.InviteCode;
import com.purchase.entity.MobileInfo;
import com.purchase.entity.User;
import com.purchase.entity.UserAddress;
import com.purchase.entity.WxUser;
import com.purchase.mapper.AppUserMapper;
import com.purchase.mapper.InviteCodeMapper;
import com.purchase.mapper.InviteMapper;
import com.purchase.mapper.MobileInfoMapper;
import com.purchase.mapper.UserAddressMapper;
import com.purchase.mapper.UserMapper;
import com.purchase.mapper.WxUserMapper;
import com.purchase.service.InviteCodeService;
import com.purchase.service.UserService;
import com.purchase.util.AliIM;
import com.purchase.util.AuthUtil;
import com.purchase.util.DateUtil;
import com.purchase.util.JavaWebToken;
import com.purchase.util.LogInfo;
import com.purchase.util.MyBeanUtils;
import com.purchase.util.RedisUtil;
import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

	private static ObjectMapper jsonMapper = new ObjectMapper();

	@Resource
	UserMapper userMapper;

	@Resource
	UserAddressMapper userAddressMapper;

	@Resource
	WxUserMapper wxUserMapper;

	@Resource
	RedisUtil redisUtil;

	@Resource
	InviteMapper inviteMapper;

	@Resource
	InviteCodeMapper inviteCodeMapper;

	@Resource
	MobileInfoMapper mobileInfoMapper;

	@Resource
	InviteCodeService InviteCodeService;
	
	@Autowired
	AppUserMapper appUserMapper;

	@Transactional(rollbackFor = Exception.class)
	@Override
	public ResponseForm addUer(RequestForm param) {
		ResponseForm result = new ResponseForm();
		Map<String, Object> paramMap = (Map<String, Object>) param.getData();
		try {
			User user = new User();
			MyBeanUtils.populate(user, paramMap);
			user.setCreateTime(new Date());
			if (!StringUtils.isNotBlank(user.getName())) {
				result.setStatus(false);
				result.setMessage(LogInfo.USER_NAME_LOG);
				log.error(LogInfo.USER_NAME_LOG);
				return result;
			}
			if (!StringUtils.isNotBlank(user.getPassword())) {
				result.setStatus(false);
				result.setMessage(LogInfo.USER_PASSWORD_LOG);
				log.error(LogInfo.USER_PASSWORD_LOG);
				return result;
			}
			// user.set
			userMapper.insertSelective(user);

			// result.setData(list);
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error(LogInfo.ERROR);
		}
		return result;
	}

	@Override
	public ResponseForm checkUerName(RequestForm param) {
		ResponseForm result = new ResponseForm();
		Map<String, Object> paramMap = (Map<String, Object>) param.getData();
		try {
			User user = new User();
			MyBeanUtils.populate(user, paramMap);
			// user.setName(String.valueOf(paramMap.get("name")));
			user = userMapper.selectOne(user);
			if (user != null) {
				result.setStatus(true);
				result.setCode(LogInfo.ERROR_CODE);
				result.setMessage(LogInfo.USER_CHECK_LOG);
				log.error(LogInfo.USER_CHECK_LOG);
				return result;
			}
			result.setMessage(LogInfo.USER_CHECK_SUCCESS);
			result.setCode(LogInfo.SUCCESS_CODE);
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error(LogInfo.ERROR);
		}
		return result;
	}

	@Override
	public ResponseForm login(RequestForm param) {
		ResponseForm result = new ResponseForm();
		Map<String, Object> paramMap = (Map<String, Object>) param.getData();
		try {
			User user = new User();
			// MyBeanUtils.populate(user, paramMap);
			user.setName(String.valueOf(paramMap.get("name")));
			user.setStatus(0);
			user = userMapper.selectOne(user);
			if (user == null) {
				result.setStatus(false);
				result.setCode(LogInfo.ERROR_CODE);
				result.setMessage(LogInfo.USER_ERROR);
				log.error(LogInfo.USER_ERROR);
				return result;
			}
			if (!user.getPassword().endsWith(String.valueOf(paramMap.get("password")))) {
				result.setStatus(false);
				result.setCode(LogInfo.ERROR_CODE);
				result.setMessage(LogInfo.PASSWORD_ERROR);
				log.error(LogInfo.PASSWORD_ERROR);
				return result;
			}

			Map<String, Object> loginInfo = new HashMap<>();
			loginInfo.put("userId", user.getId());
			String sessionId = JavaWebToken.createJavaWebToken(loginInfo);
			System.out.println("sessionID" + sessionId);
			user.setSessionId(sessionId);

			result.setData(user);
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error(LogInfo.ERROR);
		}
		return result;
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public ResponseForm update(RequestForm param, HttpServletRequest request) {
		ResponseForm result = new ResponseForm();

		// 登录认证
		// try {
		// Long userId = AuthUtil.getUserId(request);
		// } catch (Exception e1) {
		// // TODO Auto-generated catch block
		// e1.printStackTrace();
		// result.setStatus(false);
		// result.setCode("500");
		// result.setMessage(LogInfo.AUTH_ERROR);
		// log.error(LogInfo.AUTH_ERROR);
		// return result;
		// }

		Map<String, Object> paramMap = (Map<String, Object>) param.getData();
		try {

			User user = new User();
			MyBeanUtils.populate(user, paramMap);
			// user.setName(String.valueOf(paramMap.get("name")));

			userMapper.updateByPrimaryKeySelective(user);

		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error(LogInfo.ERROR);
		}
		return result;
	}

	@Override
	public ResponseForm getUserAddress(RequestForm param) {
		ResponseForm result = new ResponseForm();
		Map<String, Object> paramMap = (Map<String, Object>) param.getData();
		try {
			String wxUid = (String) paramMap.get("wxUid");
			if (!StringUtils.isNotBlank(wxUid)) {
				result.setStatus(false);
				result.setCode(LogInfo.ERROR_CODE);
				result.setMessage(LogInfo.USER_ERROR);
				log.error(LogInfo.USER_ERROR);
				return result;
			}

			Example example = new Example(UserAddress.class);
			example.setOrderByClause("default_options DESC");
			Criteria criteria = example.createCriteria();
			criteria.andEqualTo("wxUid", wxUid);
			criteria.andEqualTo("status", 0);
			List<UserAddress> list = userAddressMapper.selectByExample(example);

			result.setData(list);
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error(LogInfo.ERROR);
		}
		return result;
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public ResponseForm addUserAddress(RequestForm param) {
		ResponseForm result = new ResponseForm();
		Map<String, Object> paramMap = (Map<String, Object>) param.getData();
		try {
			String wxUid = (String) paramMap.get("wxUid");
			if (!StringUtils.isNotBlank(wxUid)) {
				result.setStatus(false);
				result.setCode(LogInfo.ERROR_CODE);
				result.setMessage(LogInfo.USER_ERROR);
				log.error(LogInfo.USER_ERROR);
				return result;
			}
			
			UserAddress userAddress = new UserAddress();
			MyBeanUtils.populate(userAddress, paramMap);
			
			/*如果将当前地址设置为默认地址,则将所有地址改为非默认地址*/
			if(userAddress.getDefaultOptions()!=null&&userAddress.getDefaultOptions()==1) {
				UserAddress pojo = new UserAddress();
				pojo.setDefaultOptions(0);
				Example example = new Example(UserAddress.class);
				example.createCriteria().andEqualTo("wxUid", wxUid);
				userAddressMapper.updateByExampleSelective(pojo, example);
			}
			
			userAddress.setCreateTime(new Date());
			userAddressMapper.insertSelective(userAddress);

			// User user = new User();
			// MyBeanUtils.populate(user, paramMap);
			// //user.setName(String.valueOf(paramMap.get("name")));
			// userMapper.updateByPrimaryKeySelective(user);

		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error(LogInfo.ERROR);
		}
		return result;
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public ResponseForm updateUserAddress(RequestForm param) {
		ResponseForm result = new ResponseForm();
		Map<String, Object> paramMap = (Map<String, Object>) param.getData();
		try {
			String wxUid = (String) paramMap.get("wxUid");

			if (!StringUtils.isNotBlank(wxUid)) {
				result.setStatus(false);
				result.setCode(LogInfo.ERROR_CODE);
				result.setMessage(LogInfo.USER_ERROR);
				log.error(LogInfo.USER_ERROR);
				return result;
			}

			UserAddress userAddress = new UserAddress();
			MyBeanUtils.populate(userAddress, paramMap);
			if (userAddress.getDefaultOptions() != null && userAddress.getDefaultOptions() == 1) {
				userAddressMapper.updateDefaultOptions(wxUid);
			}
			userAddressMapper.updateByPrimaryKeySelective(userAddress);
			// userAddressMapper.updateByExampleSelective(record, example)

		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error(LogInfo.ERROR);
		}
		return result;
	}

	@Override
	public ResponseForm getUserAddressById(RequestForm param) {
		ResponseForm result = new ResponseForm();
		Map<String, Object> paramMap = (Map<String, Object>) param.getData();
		try {
			String addressId = (String) paramMap.get("id");
			if (!StringUtils.isNotBlank(addressId)) {
				result.setStatus(false);
				result.setCode(LogInfo.PARAM_ERROR);
				result.setMessage(LogInfo.USER_ERROR);
				log.error(LogInfo.USER_ERROR);
				return result;
			}
			UserAddress userAddress = new UserAddress();
			MyBeanUtils.populate(userAddress, paramMap);
			userAddress = userAddressMapper.selectOne(userAddress);
			result.setData(userAddress);
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error(LogInfo.ERROR);
		}
		return result;
	}

	@Override
	public ResponseForm listUser(RequestForm param) {
		ResponseForm result = new ResponseForm();
		Map<String, Object> paramMap = (Map<String, Object>) param.getData();
		try {
			User user = new User();
			// MyBeanUtils.populate(user, paramMap);
			String userId = String.valueOf(paramMap.get("userId"));
			user.setId(Integer.parseInt(userId));
			user.setStatus(0);
			user = userMapper.selectOne(user);
			if (user == null) {
				result.setStatus(false);
				result.setCode(LogInfo.ERROR_CODE);
				result.setMessage(LogInfo.USER_ERROR);
				log.error(LogInfo.USER_ERROR);
				return result;
			}
			if (user.getRoleId() != 2) {
				result.setStatus(false);
				result.setCode(LogInfo.ERROR_CODE);
				result.setMessage("权限不够");
				log.error(LogInfo.USER_ERROR);
				return result;
			}
			com.purchase.util.Page.parsePage(paramMap);

			Page<Object> pageHelper = PageHelper.startPage((int) paramMap.get("page"), (int) paramMap.get("size"));

			Example example = new Example(User.class);
			example.createCriteria().andEqualTo("roleId", "1");
			List<User> userList = userMapper.selectByExample(example);
			result.setData(userList);
			result.setTotal((int) pageHelper.getTotal());
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error(LogInfo.ERROR);
		}
		return result;
	}

	@Override
	public ResponseForm getUserById(RequestForm param) {
		ResponseForm result = new ResponseForm();
		Map<String, Object> paramMap = (Map<String, Object>) param.getData();
		try {
			User user = new User();
			// MyBeanUtils.populate(user, paramMap);
			String userId = String.valueOf(paramMap.get("userId"));
			if (!StringUtils.isNotBlank(userId)) {
				result.setStatus(false);
				result.setCode(LogInfo.ERROR_CODE);
				result.setMessage(LogInfo.PARAM_ERROR);
				log.error(LogInfo.USER_ERROR);
				return result;
			}

			user.setId(Integer.parseInt(userId));
			user.setStatus(0);
			user = userMapper.selectOne(user);
			result.setData(user);

		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error(LogInfo.ERROR);
		}
		return result;
	}

	@Override
	public ResponseForm listUserByStoreNo(RequestForm param) {
		ResponseForm result = new ResponseForm();
		Map<String, Object> paramMap = (Map<String, Object>) param.getData();
		try {
			User user = new User();
			// MyBeanUtils.populate(user, paramMap);
			String storeNo = String.valueOf(paramMap.get("storeNo"));
			String userId = String.valueOf(paramMap.get("userId"));
			user.setId(Integer.parseInt(userId));
			user.setStatus(0);
			user = userMapper.selectOne(user);
			if (user == null) {
				result.setStatus(false);
				result.setCode(LogInfo.ERROR_CODE);
				result.setMessage(LogInfo.USER_ERROR);
				log.error(LogInfo.USER_ERROR);
				return result;
			}

			com.purchase.util.Page.parsePage(paramMap);

			Page<Object> pageHelper = PageHelper.startPage((int) paramMap.get("page"), (int) paramMap.get("size"));

			Example example = new Example(User.class);
			example.createCriteria().andEqualTo("storeNo", storeNo).andEqualTo("roleId", 1);
			List<User> userList = userMapper.selectByExample(example);
			result.setData(userList);
			result.setTotal((int) pageHelper.getTotal());
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error(LogInfo.ERROR);
		}
		return result;
	}

	@Override
	public ResponseForm sendVerifyCode(RequestForm param) {
		ResponseForm result = new ResponseForm();
		if (param.getData() != null) {
			Map paramMap = (Map) param.getData();
			String verifyPhone = (String) paramMap.get("verifyPhone");// 验证的手机号码
			// String wxUid = (String) paramMap.get("wxUid");
			// if(StringUtils.isBlank(wxUid)) {
			// System.err.println("下发验证码失败(可能原因:信息获取失败...)");
			// log.error("下发验证码失败(可能原因:信息获取失败...)");
			// result.setMessage("下发验证码失败(可能原因:信息获取失败...)");
			// result.setStatus(false);
			// return result;
			// }
			String verifyCode = AliIM.getVerifyCode(6);
			Boolean flag = AliIM.shortMessage(verifyPhone, verifyCode);
			if (!flag) {
				System.err.println("下发验证码失败(可能原因:验证码次数已达上限...)");
				log.error("下发验证码失败(可能原因:验证码次数已达上限...)");
				result.setMessage("下发验证码失败(可能原因:验证码次数已达上限...)");
				result.setStatus(flag);
				return result;
			}
			result.setMessage("下发成功,由于验证码下发数量有限,请不要多次发送");
			redisUtil.set("verify_" + verifyPhone, verifyCode, 300);
		}
		return result;
	}

	@Transactional
	@Override
	public ResponseForm submitVerifyCode(RequestForm param) {
		ResponseForm result = new ResponseForm();
		if (param.getData() != null) {
			Map paramMap = (Map) param.getData();
			String verifyPhone = (String) paramMap.get("verifyPhone");// 验证手机号
			String wxUid = (String) paramMap.get("wxUid");// 微信用户id
			String verifyCode = (String) paramMap.get("verifyCode");// 验证码
			String verifyCode2 = redisUtil.get("verify_" + verifyPhone);
			String inviteCode = (String) paramMap.get("inviteCode");
			if (StringUtils.isBlank(wxUid)) {
				System.err.println("错误,(可能原因信息获取失败...)");
				log.error("错误,(可能原因信息获取失败...)");
				result.setMessage("错误,(可能原因信息获取失败...");
				result.setStatus(false);
				return result;
			}
			if (inviteCode != null) {
				Invite invite = new Invite();
				invite.setInviteCode(inviteCode);
				List<Invite> list = inviteMapper.select(invite);
				if (list == null || list.size() < 1) {
					result.setMessage("无效的邀请码");
					result.setStatus(false);
					log.error("无效的邀请码");
					result.setCode("error");
					return result;
				}
			}
			if (verifyCode2 == null) {
				result.setMessage("验证码已失效,重新获取");
				result.setStatus(false);
				log.error("验证码已失效,重新获取");
				result.setCode("expired");
				return result;
			} else if (!verifyCode.contains(verifyCode2)) {
				result.setMessage("验证码错误");
				result.setStatus(false);
				log.error("验证码错误");
				result.setCode("error");
				return result;
			}
			try {
				wxUserMapper.updateVerifyPhone(paramMap);
				WxUser wxUser = new WxUser();
				wxUser.setWxUid(wxUid);
				wxUser = wxUserMapper.selectOne(wxUser);
				Map<String, Object> loginInfo = new HashMap<>();
				if (wxUser == null || wxUser.getUserId() == null) {
					result.setMessage("用户信息错误(建议:删除小程序重新进入)");
					result.setStatus(false);
					log.error("用户信息错误(建议:删除小程序重新进入)");
					return result;
				}
				loginInfo.put("userId", wxUser.getUserId());
				String sessionId = JavaWebToken.createJavaWebToken(loginInfo);
				loginInfo.remove("userId");
				loginInfo.put("sessionId", sessionId);
				result.setCode("success");
				result.setData(loginInfo);
			} catch (Exception e) {
				result.setMessage("更新信息失败");
				result.setStatus(false);
				log.error("更新信息失败");
				e.printStackTrace();
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			}
		}
		return result;
	}

	@Override
	public ResponseForm checkVerifyPhone(RequestForm param) {
		ResponseForm result = new ResponseForm();
		if (param.getData() != null) {
			Map paramMap = (Map) param.getData();
			String verifyPhone = (String) paramMap.get("verifyPhone");
			WxUser wxUserByWxUid = new WxUser();
			String wxUid = paramMap.get("wxUid").toString();
			if (StringUtils.isBlank(wxUid)) {
				result.setMessage("用户信息获取失败...");
				result.setStatus(false);
				result.setCode("less");
				return result;
			}
			wxUserByWxUid.setWxUid(wxUid);
			try {
				int i = wxUserMapper.checkUsedPhone(paramMap);
				if (i > 0) {
					result.setMessage("该手机号已被使用,请更换");
					result.setStatus(false);
					result.setCode("occupied");
					return result;
				}

				wxUserByWxUid = wxUserMapper.selectOne(wxUserByWxUid);
				if (wxUserByWxUid != null && wxUserByWxUid.getVerifyPhone() != null
						&& !verifyPhone.contains(wxUserByWxUid.getVerifyPhone())) {
					result.setMessage("当前用户已绑定过手机号");
					result.setStatus(false);
					result.setCode("bounded");
					StringBuilder strBuilder = new StringBuilder(wxUserByWxUid.getVerifyPhone());
					paramMap.clear();
					paramMap.put("verifiedPhone", strBuilder.replace(3, 7, "****"));
					result.setData(paramMap);
					return result;
				}

				if (wxUserByWxUid == null || StringUtils.isBlank(wxUserByWxUid.getVerifyPhone())) {
					result.setMessage("当前用户初次验证手机号");
				}
				result.setCode("usable");
			} catch (Exception e) {
				result.setCode("400");
				result.setMessage("参数错误");
				result.setStatus(false);
				log.error("参数错误");
				e.printStackTrace();
			}
		}
		return result;
	}

	@Override
	public ResponseForm checkAndroidPhone(RequestForm param) {
		ResponseForm result = new ResponseForm();
		if (param.getData() != null) {
			Map paramMap = (Map) param.getData();
			try {
				WxUser wxUser = new WxUser();
				wxUser.setVerifyPhone((String) paramMap.get("verifyPhone"));
				List<WxUser> num = wxUserMapper.select(wxUser);
				if (num != null && num.size() > 0) {
					result.setMessage("该手机号已经注册,如果是您本人,请直接使用密码或验证码登录");
					result.setStatus(false);
					log.error("该手机号已经注册,如果是您本人,请直接使用密码或验证码登录");
					return result;
				}
			} catch (Exception e) {
				result.setCode("400");
				result.setMessage("参数错误");
				result.setStatus(false);
				log.error("参数错误");
				e.printStackTrace();
			}
		}
		return result;
	}

	@Transactional
	@Override
	public ResponseForm registerAndroidUser(RequestForm param) {
		ResponseForm result = new ResponseForm();
		if (param.getData() != null) {
			Map paramMap = (Map) param.getData();
			Map resultMap = new HashMap<>();
			String verifyPhone = (String) paramMap.get("verifyPhone");
			String password = (String) paramMap.get("password");
			String inviteCode = (String) paramMap.get("inviteCode");
			InviteCode InviteCodePojo = new InviteCode();
			/* 验证码校验 */
			try {
				String verifyCode = redisUtil.get("verify_" + verifyPhone);
				if (StringUtils.isBlank(verifyCode)) {
					result.setMessage("验证码已失效,重新获取");
					result.setStatus(false);
					log.error("验证码已失效,重新获取");
					result.setCode("expired");
					return result;
				} else if (!verifyCode.equals(paramMap.get("verifyCode").toString())) {
					result.setMessage("验证码错误");
					result.setStatus(false);
					log.error("验证码错误");
					result.setCode("error");
					return result;
				}
			} catch (Exception e) {
				e.printStackTrace();
				result.setCode("400");
				result.setMessage("error");
				result.setStatus(false);
				log.error("[redis]:data query error");
				return result;
			}

			/* 邀请码校验 */
			try {
				InviteCodePojo.setInviteCode(inviteCode);
				InviteCodePojo = inviteCodeMapper.selectOne(InviteCodePojo);
				Date now = new Date();
				if (InviteCodePojo == null || InviteCodePojo.getCodeStatus() == 0) {
					result.setMessage("无效邀请码");
					result.setStatus(false);
					log.error("无效邀请码");
					result.setCode("expired");
					return result;
				}
			} catch (Exception e) {
				e.printStackTrace();
				result.setCode("400");
				result.setMessage("error");
				result.setStatus(false);
				log.error("[mysql]:data query error");
				return result;
			}

			/* 校验通过,创建用户 */
			WxUser wxUser = MyBeanUtils.populate(new WxUser(), paramMap);
			wxUser.setWxUid(DateUtil.getUUID());
			try {
				wxUserMapper.insertSelective(wxUser);
				Map<String, Object> loginInfo = new HashMap<>();
				loginInfo.put("userId", wxUser.getUserId());
				resultMap.put("sessionId", JavaWebToken.createJavaWebToken(loginInfo));
				resultMap.put("wxUid", wxUser.getWxUid());
				result.setData(resultMap);
			} catch (Exception e) {
				e.printStackTrace();
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				result.setMessage("参数错误");
				result.setStatus(false);
				result.setCode("400");
				log.error("[mysql]:data query error");
				return result;
			}

			/* 邀请码已使用 */
			InviteCodePojo.setCodeStatus(0);
			inviteCodeMapper.updateByPrimaryKeySelective(InviteCodePojo);

			/* 注册用户的来源 */
			try {
				MobileInfo mobileInfo = MyBeanUtils.populate(new MobileInfo(), paramMap);
				mobileInfo.setWxUid(wxUser.getWxUid());
				mobileInfoMapper.insertSelective(mobileInfo);
			} catch (Exception e) {
				e.printStackTrace();
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				result.setMessage("参数错误");
				result.setStatus(false);
				result.setCode("400");
				log.error("[mysql]:data query error");
				return result;
			}
			InviteCodeService.useCode(wxUser.getWxUid(), inviteCode);
		}
		return result;
	}

	@Transactional
	@Override
	public ResponseForm register2Login(RequestForm param) {
		ResponseForm result = new ResponseForm();
		if (param.getData() != null) {
			Map paramMap = (Map) param.getData();
			Map resultMap = new HashMap<>();
			String verifyPhone = (String) paramMap.get("verifyPhone");
			// String password = (String) paramMap.get("password");
			String inviteCode = (String) paramMap.get("inviteCode");
			// InviteCode InviteCodePojo = new InviteCode();
			try {
				/* 验证码校验 */
				String verifyCode = redisUtil.get("verify_" + verifyPhone);
				if (StringUtils.isBlank(verifyCode)) {
					result.setMessage("验证码已失效,重新获取");
					result.setStatus(false);
					log.error("验证码已失效,重新获取");
					result.setCode("expired");
					return result;
				} else if (!verifyCode.equals(paramMap.get("verifyCode").toString())) {
					result.setMessage("验证码错误");
					result.setStatus(false);
					log.error("验证码错误");
					result.setCode("error");
					return result;
				}

				/* 推广码校验 */
				// WxUser user = new WxUser();
				// user.setVerifyPhone(inviteCode);
				// user = wxUserMapper.selectOne(user);
				// if(user==null) {
				// result.setStatus(false);
				// result.setCode("400");
				// result.setMessage("推广用户不存在!");
				// return result;
				// }

				/* 校验手机号 */
				WxUser wxUser = new WxUser();
				wxUser.setVerifyPhone(verifyPhone);
				wxUser = wxUserMapper.selectOne(wxUser);
				if(wxUser==null) {
					wxUser = new WxUser(verifyPhone);
					wxUser.setWxUid(DateUtil.getUUID());
					wxUserMapper.insertSelective(wxUser);
				}
				/* 获取sessionId并返回 */
				Map<String, Object> loginInfo = new HashMap<>();
				loginInfo.put("userId", wxUser.getUserId());
				resultMap.put("sessionId", JavaWebToken.createJavaWebToken(loginInfo));
				resultMap.put("wxUid", wxUser.getWxUid());
				result.setData(resultMap);
			} catch (Exception e) {
				e.printStackTrace();
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				result.setMessage("参数错误");
				result.setStatus(false);
				result.setCode("400");
				log.error("[mysql]:data query error");
				return result;
			}
		}
		return result;
	}

	@Override
	public ResponseForm androidLoginByPwd(RequestForm param) {
		ResponseForm result = new ResponseForm();
		if (param.getData() != null) {
			Map paramMap = (Map) param.getData();
			// Map resultMap = new HashMap<>();
			// WxUser wxUser = MyBeanUtils.populate(new WxUser(), paramMap);
			WxUser wxUser = new WxUser();
			String verifyPhone = (String) paramMap.get("verifyPhone");
			String password = (String) paramMap.get("password");
			try {
				wxUser.setVerifyPhone(verifyPhone);
				wxUser = wxUserMapper.selectOne(wxUser);
				if (wxUser == null) {
					result.setCode("400");
					result.setMessage("用户不存在!");
					result.setStatus(false);
					log.error("用户不存在!");
					return result;
				}
				if (StringUtils.isBlank(wxUser.getPassword())) {
					result.setCode("400");
					result.setMessage("您还未设置过密码,请使用验证码登录!");
					result.setStatus(false);
					log.error("您还未设置过密码,请使用验证码登录!");
					return result;
				}
				if (StringUtils.isBlank(password) || !password.equals(wxUser.getPassword())) {
					result.setCode("400");
					result.setData("400");
					result.setMessage("密码错误,重新输入!");
					result.setStatus(false);
					log.error("密码错误,重新输入!");
					return result;
				}
				Map<String, Object> loginInfo = new HashMap<>();
				loginInfo.put("userId", wxUser.getUserId());
				Map resultMap = MyBeanUtils.describeAndNotBlank(wxUser);
				resultMap.put("sessionId", JavaWebToken.createJavaWebToken(loginInfo));
				// resultMap.put("wxUid", wxUser.getWxUid());
				result.setData(resultMap);
			} catch (Exception e) {
				e.printStackTrace();
				result.setCode("400");
				result.setMessage("参数错误");
				result.setStatus(false);
				log.error("mysql data error");
				return result;
			}
		}
		return result;
	}

	@Override
	public ResponseForm androidLoginByCode(RequestForm param) {
		ResponseForm result = new ResponseForm();
		if (param.getData() != null) {
			Map paramMap = (Map) param.getData();
			String verifyPhone = (String) paramMap.get("verifyPhone");
			// WxUser wxUser = MyBeanUtils.populate(new WxUser(), paramMap);
			WxUser wxUser = new WxUser();
			Map resultMap = new HashMap<>();
			try {
				wxUser.setVerifyPhone(verifyPhone);
				wxUser = wxUserMapper.selectOne(wxUser);
				if (wxUser == null) {
					result.setCode("400");
					result.setMessage("用户未注册");
					result.setStatus(false);
					log.error("用户未注册");
					return result;
				}
				resultMap = MyBeanUtils.describeAndNotBlank(wxUser);
			} catch (Exception e) {
				e.printStackTrace();
				result.setCode("400");
				result.setMessage("参数错误");
				result.setStatus(false);
				log.error("[mysql]:data query error");
				return result;
			}
			try {
				String verifyCode = redisUtil.get("verify_" + wxUser.getVerifyPhone());
				if (StringUtils.isBlank(verifyCode)) {
					result.setMessage("验证码已失效,重新获取");
					result.setStatus(false);
					log.error("验证码已失效,重新获取");
					result.setCode("expired");
					return result;
				} else if (!verifyCode.equals(paramMap.get("verifyCode").toString())) {
					result.setMessage("验证码错误");
					result.setStatus(false);
					log.error("验证码错误");
					result.setCode("error");
					return result;
				}
			} catch (Exception e) {
				e.printStackTrace();
				result.setCode("400");
				result.setMessage("error");
				result.setStatus(false);
				log.error("[redis]:data query error");
				return result;
			}
			Map<String, Object> loginInfo = new HashMap<>();
			loginInfo.put("userId", wxUser.getUserId());
			resultMap.put("sessionId", JavaWebToken.createJavaWebToken(loginInfo));
			// resultMap.put("wxUid", wxUser.getWxUid());
			result.setData(resultMap);
		}
		return result;
	}

	@Transactional
	@Override
	public ResponseForm modifyPwdByCode(RequestForm param) {
		ResponseForm result = new ResponseForm();
		if (param.getData() != null) {
			Map paramMap = (Map) param.getData();
			String verifyPhone = (String) paramMap.get("verifyPhone");
			String submitCode = (String) paramMap.get("verifyCode");
			String newPwd = (String) paramMap.get("password");
			/* 验证码校验 */
			try {
				String verifyCode = redisUtil.get("verify_" + verifyPhone);
				if (!StringUtils.equals(submitCode, verifyCode)) {
					result.setMessage("验证码错误");
					result.setStatus(false);
					log.error("验证码错误");
					result.setCode("error");
					return result;
				}
			} catch (Exception e) {
				result.setCode("400");
				e.printStackTrace();
				result.setMessage("error");
				result.setStatus(false);
				log.error("[redis]:data query error");
				return result;
			}
			/* 密码更新 */
			try {
				WxUser userInfo = new WxUser();
				userInfo.setVerifyPhone(verifyPhone);
				userInfo = wxUserMapper.selectOne(userInfo);
				/* 用户是否已注册 */
				if (userInfo == null) {
					result.setCode("400");
					result.setMessage("该手机号没有绑定用户,请先注册");
					result.setStatus(false);
					log.error("该手机号没有绑定用户,请先注册");
					return result;
				}
				userInfo.setPassword(newPwd);
				wxUserMapper.updateByPrimaryKeySelective(userInfo);
			} catch (Exception e) {
				e.printStackTrace();
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				result.setCode("400");
				result.setMessage("error");
				result.setStatus(false);
				log.error("[mysql]:data query error");
				return result;
			}

		}
		return result;
	}

	@Transactional
	@Override
	public ResponseForm modifyPwd(RequestForm param) {
		ResponseForm result = new ResponseForm();
		if (param.getData() != null) {
			Map paramMap = (Map) param.getData();
			String oldPassword = (String) paramMap.get("oldPassword");
			String newPassword = (String) paramMap.get("oldPassword");
			String verifyPhone = (String) paramMap.get("verifyPhone");
			String wxUid = (String) paramMap.get("wxUid");
			WxUser wxUser = new WxUser();
			try {
				wxUser.setVerifyPhone(verifyPhone);
				wxUser.setWxUid(wxUid);
				wxUser = wxUserMapper.selectOne(wxUser);
				if (wxUser == null) {
					result.setMessage("用户不存在!");
					result.setStatus(false);
					result.setCode("400");
					return result;
				}
				/* 如果oldPasswordb不为空说明操作是修改密码,需要验证原始密码,为空说明第一次添加密码无需验证原始密码 */
				if (StringUtils.isNotBlank(oldPassword) && oldPassword.equals(wxUser.getPassword())) {
					result.setMessage("原始密码不正确!");
					result.setStatus(false);
					result.setCode("400");
					return result;
				}
				wxUser.setPassword(newPassword);
				wxUserMapper.updateByPrimaryKeySelective(wxUser);
				result.setCode("200");
				result.setStatus(true);
				result.setMessage("密码修改成功!");
			} catch (Exception e) {
				e.printStackTrace();
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				result.setCode("400");
				result.setMessage("error");
				result.setStatus(false);
				log.error("[mysql]:data query error");
				return result;
			}

		}
		return result;
	}

	@Transactional
	@Override
	public ResponseForm bindPhone(RequestForm param) {
		ResponseForm result = new ResponseForm();
		if (param.getData() != null) {
			Map paramMap = (Map) param.getData();
			Map resultMap = new HashMap<>();
			String wxUid = (String) paramMap.get("wxUid");
			String verifyPhone = (String) paramMap.get("verifyPhone");
			try {
				/* 验证码校验 */
				String verifyCode = redisUtil.get("verify_" + verifyPhone);
				if (StringUtils.isBlank(verifyCode)||!verifyCode.equals(paramMap.get("verifyCode").toString())) {
					result.setCode("400");
					result.setStatus(false);
					result.setMessage("验证码错误或已失效");
					log.error("验证码错误或已失效");
					return result;
				}
				/*验证手机号*/
				WxUser user = new WxUser();
				user.setVerifyPhone(verifyPhone);
				user = wxUserMapper.selectOne(user);
				if(user!=null&&!wxUid.equals(user.getWxUid())) {
					result.setCode("400");
					result.setStatus(false);
					result.setMessage("该手机号已被其他用户绑定,请更换其它手机号");
					return result;
				}
				/*验证用户*/
				user = new WxUser();
				user.setWxUid(wxUid);
				user = wxUserMapper.selectOne(user);
				if(user==null) {
					result.setCode("400");
					result.setStatus(false);
					result.setMessage("当前用户不存在,请重新登录");
					return result;
				}
				else if(StringUtils.isNotBlank(user.getVerifyPhone())&&!user.getVerifyPhone().equals(verifyPhone)) {
					result.setCode("400");
					result.setStatus(false);
					result.setMessage("您已绑定手机号与当前输入手机号不一致");
					return result;
				}
				else if(StringUtils.isNotBlank(user.getVerifyPhone())&&user.getVerifyPhone().equals(verifyPhone)) {
					result.setCode("200");
					result.setStatus(true);
					result.setMessage("操作成功!");
					return result;
				}
				else if(StringUtils.isBlank(user.getVerifyPhone())) {
					user.setVerifyPhone(verifyPhone);
					wxUserMapper.updateByPrimaryKeySelective(user);
					result.setCode("200");
					result.setStatus(true);
					result.setMessage("操作成功!");
				}
			} catch (Exception e) {
				e.printStackTrace();
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				result.setMessage("参数错误");
				result.setStatus(false);
				result.setCode("500");
				log.error("[mysql]:data query error");
			}
		}
		return result;
	}

	@SuppressWarnings({ "unused", "rawtypes" })
	@Override
	public ResponseForm getInviterInfo(RequestForm param) {
		ResponseForm result = new ResponseForm();
		if(param.getData()!=null) {
			Map paramMap = (Map) param.getData();
			String inviteCode = (String) paramMap.get("parentInviteCode");
			AppUser appUser = new AppUser();
			appUser.setInviteCode(inviteCode);
			appUser = appUserMapper.selectOne(appUser);
			result.succ("操作成功", appUser);
		}else{
			result.fail("400", "参数为空");
		}
		return result;
	}
}
