package com.purchase.service.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.beanutils.PropertyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.purchase.entity.AppUser;
import com.purchase.entity.AppVersion;
import com.purchase.entity.IosOnline;
import com.purchase.mapper.AppUserMapper;
import com.purchase.mapper.AppVersionMapper;
import com.purchase.mapper.IosOnlineMapper;
import com.purchase.service.AppVerService;
import com.purchase.util.JavaWebToken;
import com.purchase.util.MyBeanUtils;
import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AppVerServiceImpl implements AppVerService {
	@Resource
	AppVersionMapper appVerMapper;
	@Autowired
	IosOnlineMapper iosOnlineMapper;
	@Autowired
	AppUserMapper appUserMapper;

	@Override
	public ResponseForm getVerInfo(RequestForm param) {
		ResponseForm result = new ResponseForm();
		if (param.getData() != null) {
			Map paramMap = (Map) param.getData();
			// String currentVerCode = (String) paramMap.get("VerCode");
			String appAlias = (String) paramMap.get("appAlias");
			try {
				AppVersion verInfo = appVerMapper.getLatestVer(appAlias);
				// if(app.getVerCode().equals(currentVerCode)) {
				// result.setMessage("当前程序版本已是最新版本");
				// return result;
				// }
				result.setCode("200");
				result.setStatus(true);
				result.setData(verInfo);
				// result.setMessage("发现新版本待更新,安装包大小为"+app.getAppSize()+"M");
				result.setMessage("操作成功");
			} catch (Exception e) {
				result.setCode("400");
				result.setStatus(true);
				result.setMessage("服务器错误");
				log.error("sql错误", e);
				e.printStackTrace();
			}
		}
		return result;
	}

	@Transactional
	@Override
	public ResponseForm newVer(RequestForm param) {
		ResponseForm result = new ResponseForm();
		if (param.getData() != null) {
			Map paramMap = (Map) param.getData();
			AppVersion ver = MyBeanUtils.populate(new AppVersion(), paramMap);
			ver.setVerCode(UUID.randomUUID().toString().replace("-", ""));
			try {
				appVerMapper.insertSelective(ver);
				result.setCode("200");
				result.setStatus(true);
				result.setMessage("操作成功");
			} catch (Exception e) {
				result.setCode("400");
				result.setStatus(true);
				result.setMessage("服务器错误");
				log.error("sql错误", e);
				e.printStackTrace();
			}
		}
		return result;
	}

	/**
	 * IOS端app是否上线状态强求
	 * 
	 * @param null
	 * @return isOnline
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public ResponseForm isOnline() {
		ResponseForm result = new ResponseForm();
		Map resultMap = new HashMap<>();
		try {
			IosOnline iosOnline = iosOnlineMapper.selectByPrimaryKey(1);
			resultMap.put("isOnline", iosOnline.getIsOnline() == 1 ? "true" : "false");
			resultMap.put("version", iosOnline.getVersion());
			result.succ("操作成功", resultMap);
		} catch (Exception e) {
			resultMap.put("isOnline", "false");
			result.fail("400", "服务器错误", resultMap);
			log.error("sql错误", e);
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * IOS端游客访问 tourist
	 * 
	 * @param null
	 * @return 用户信息
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public ResponseForm touristVisit() {
		ResponseForm result = new ResponseForm();
		try {
			AppUser appUser = new AppUser();
			appUser.setUserStatus(10);
			appUser = appUserMapper.selectOne(appUser);
			if (appUser == null) {
				result.fail("400", "游客访问服务暂时无法使用");
				return result;
			} else {
				Map resultMap = MyBeanUtils.describeAndNotBlank(appUser);
				resultMap.remove("class");
				Map<String, Object> loginInfo = new HashMap<>();
				loginInfo.put("userId", appUser.getUserId());
				resultMap.put("sessionId", JavaWebToken.createJavaWebToken(loginInfo));
				result.succ("操作成功", resultMap);
			}
		} catch (Exception e) {
			result.fail("400", "服务器错误");
			log.error("sql错误", e);
			e.printStackTrace();
		}
		return result;
	}

}
