package com.purchase.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.purchase.entity.Brand;
import com.purchase.entity.Store;
import com.purchase.entity.StoreBrand;
import com.purchase.entity.User;
import com.purchase.mapper.BrandMapper;
import com.purchase.mapper.GoodsMapper;
import com.purchase.mapper.StoreBrandMapper;
import com.purchase.mapper.StoreMapper;
import com.purchase.mapper.UserMapper;
import com.purchase.service.StoreService;
import com.purchase.util.LogInfo;
import com.purchase.util.MyBeanUtils;
import com.purchase.util.NoUtil;
import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;

@Slf4j
@Service
public class StoreServiceImpl implements StoreService {

	@Resource
	StoreMapper storeMapper;
	
	@Resource
	UserMapper userMapper;
	
	@Resource
	BrandMapper brandMapper;
	
	@Resource
	StoreBrandMapper storeBrandMapper;
	
	@Resource
	GoodsMapper goodsMapper;

	@Override
	public ResponseForm getStoreList(RequestForm param) {
		ResponseForm result = new ResponseForm();
		try {
			Map<String,Object> paramMap = (Map<String,Object>)param.getData();
//			String storeNo =(String) paramMap.get("storeNo");
//			if(!StringUtils.isNotBlank(storeNo)){
//				result.setStatus(false);
//				result.setMessage(LogInfo.PARAM_ERROR);
//				log.error(LogInfo.PARAM_ERROR);
//				return result;
//			}
			com.purchase.util.Page.parsePage(paramMap);
			Example example = new Example(Store.class);
			example.setOrderByClause("create_time DESC");
			Page<Object> pageHelper = PageHelper.startPage((int) paramMap.get("page"), (int) paramMap.get("size"));
			List<Store> list = storeMapper.selectByExample(example);
			
			for (int i = 0; i < list.size(); i++) {
				Store store= list.get(i);
				List<Map<String, Object>> bList=storeMapper.getBrandByStoreNo(store.getStoreNo());
				store.setBarndList(bList);
			}
			
			
			
//			Example example = new Example(Area.class);
//			Criteria c = example.createCriteria();
//			c.andEqualTo("pid","0" );
//			Area area = new Area();
//			area.setPid(0);
//			List<Area> list =areaMapper.select(area);
			result.setData(list);
			result.setTotal((int) pageHelper.getTotal());
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error(LogInfo.ERROR);
		}
		return result;
	}

	@Override
	public ResponseForm getStoreByStroreNo(RequestForm param) {
		ResponseForm result = new ResponseForm();
		try {
			Map<String,Object> paramMap = (Map<String,Object>)param.getData();
			String storeNo =(String) paramMap.get("storeNo");
			if(!StringUtils.isNotBlank(storeNo)){
				result.setStatus(false);
				result.setMessage(LogInfo.PARAM_ERROR);
				log.error(LogInfo.PARAM_ERROR);
				return result;
			}
			Store store = new Store();
			store.setStoreNo(storeNo);
//			Example example = new Example(Store.class);
//			Criteria c = example.createCriteria();
//			c.andEqualTo("storeNo",storeNo );
			store= storeMapper.selectOne(store);
			List<Map<String, Object>> bList=storeMapper.getBrandByStoreNo(store.getStoreNo());
			store.setBarndList(bList);
			result.setData(store);
			
			
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error(LogInfo.ERROR);
		}
		return result;
	}

	@Override
	public ResponseForm getStoreById(RequestForm param) {
		ResponseForm result = new ResponseForm();
		try {
			Map<String,Object> paramMap = (Map<String,Object>)param.getData();
			String storeId =(String) paramMap.get("storeId");
			if(!StringUtils.isNotBlank(storeId)){
				result.setStatus(false);
				result.setMessage(LogInfo.PARAM_ERROR);
				log.error(LogInfo.PARAM_ERROR);
				return result;
			}
//			Example example = new Example(Store.class);
//			Criteria c = example.createCriteria();
//			c.andEqualTo("storeNo",storeNo );
			Store store= storeMapper.selectByPrimaryKey(Integer.parseInt(storeId));
			List<Map<String, Object>> bList=storeMapper.getBrandByStoreNo(store.getStoreNo());
			store.setBarndList(bList);
			result.setData(store);
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error(LogInfo.ERROR);
		}
		return result;
	}
    @Transactional(rollbackFor=Exception.class)
	@Override
	public ResponseForm addStore(RequestForm param) {
		ResponseForm result = new ResponseForm();
		if(param.getData()!=null) {
			Map paramMap = (Map) param.getData();
			Store bean  = new Store();
			MyBeanUtils.populate(bean, paramMap);
			try {
				// 门店号设置S00000001
				PageHelper.startPage(1, 1);
				Example example = new Example(Store.class);
				example.setOrderByClause("store_no DESC");
				List<Store> stores= storeMapper.selectByExample(example);
				System.out.println(stores);
				
				String storeNo = null;
				//当size为0时,创建第一家门店,设置storeNo为S00000001
				if(stores.size()==0) {
					storeNo = "S00000001";
				}else{
					storeNo = stores.get(0).getStoreNo();
					storeNo = NoUtil.storeNoUil(storeNo);
				}
				//店长账号新增
				if(paramMap.get("mobile")==null&&paramMap.get("name")==null) {
					result.setStatus(false);
					result.setMessage(LogInfo.PARAM_ERROR);
					log.error(LogInfo.PARAM_ERROR);
					return result;
				}
				
				List brandList = (List) paramMap.get("barndList");
				if (brandList == null || brandList.size() < 0) {
					result.setStatus(false);
					result.setMessage(LogInfo.PARAM_ERROR);
					log.error(LogInfo.PARAM_ERROR);
					return result;
				}
				
				bean.setStoreNo(storeNo);
				bean.setCreateTime(new Date());
				storeMapper.insertSelective(bean);
				
				StoreBrand sBrand;
				for (int i = 0; i < brandList.size(); i++) {
					sBrand =new StoreBrand();
					String bid =(String) brandList.get(i);
					sBrand.setBrandId(Integer.valueOf(bid));
					sBrand.setStoreNo(storeNo);
					storeBrandMapper.insertSelective(sBrand);
				}
				
				//生成一个店长
				User ue = new User();
				MyBeanUtils.populate(ue, paramMap);
				ue.setStoreNo(bean.getStoreNo());
				ue.setRoleId(1);
				userMapper.insertSelective(ue);
				
				//店铺查询条件
				goodsMapper.addRateCondition(storeNo);
				goodsMapper.addSizeCondition(storeNo);
				
			} catch (Exception e) {
				e.printStackTrace();
				result.setStatus(false);
				result.setMessage(LogInfo.ERROR);
				log.error(LogInfo.ERROR);
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			}
		}
		return result;
	}
    
    @Transactional(rollbackFor=Exception.class)
	@Override
	public ResponseForm editStore(RequestForm param) {
		ResponseForm result = new ResponseForm();
			try {
				Map paramMap = (Map) param.getData();
				String storeNo =(String) paramMap.get("storeNo");
				String storeId =(String) paramMap.get("storeId");
				if(!StringUtils.isNotBlank(storeId) ||!StringUtils.isNotBlank(storeNo)  ){
					result.setStatus(false);
					result.setMessage(LogInfo.PARAM_ERROR);
					log.error(LogInfo.PARAM_ERROR);
					return result;
				}
				List brandList = (List) paramMap.get("barndList");
				if (brandList == null || brandList.size() < 0) {
					result.setStatus(false);
					result.setMessage(LogInfo.PARAM_ERROR);
					log.error(LogInfo.PARAM_ERROR);
					return result;
				}
				
				Store bean  = new Store();
				MyBeanUtils.populate(bean, paramMap);
				bean.setUpdateTime(new Date());
				storeMapper.updateByPrimaryKeySelective(bean);
				StoreBrand sBrand;
				sBrand =new StoreBrand();
				sBrand.setStoreNo(bean.getStoreNo());
				storeBrandMapper.delete(sBrand);
				
				for (int i = 0; i < brandList.size(); i++) {
					sBrand =new StoreBrand();
					String bid =(String) brandList.get(i);
					sBrand.setBrandId(Integer.valueOf(bid));
					sBrand.setStoreNo(bean.getStoreNo());
					storeBrandMapper.insertSelective(sBrand);
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				result.setStatus(false);
				result.setMessage(LogInfo.ERROR);
				log.error(LogInfo.ERROR);
			}
		return result;
	}

	@Override
	public ResponseForm getBrand(RequestForm param) {

		ResponseForm result = new ResponseForm();
		try {
//			Map<String,Object> paramMap = (Map<String,Object>)param.getData();
			Example example = new Example(Brand.class);
			example.setOrderByClause("create_time DESC");
			List<Brand> list = brandMapper.selectByExample(example);
			result.setData(list);
			
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error(LogInfo.ERROR);
		}
		return result;
	
	}

	@Override
	public ResponseForm getStores(RequestForm param) {
		ResponseForm result = new ResponseForm();
		try {
			Map<String,Object> paramMap = (Map<String,Object>)param.getData();
			
			Example example = new Example(Store.class);
			example.setOrderByClause("create_time DESC");
			List<Store> list = storeMapper.selectByExample(example);
			
			
			result.setData(list);
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error(LogInfo.ERROR);
		}
		return result;
	}


}
