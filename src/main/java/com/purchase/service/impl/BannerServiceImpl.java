package com.purchase.service.impl;

import com.purchase.entity.Banner;
import com.purchase.mapper.BannerMapper;
import com.purchase.service.BannerService;
import com.purchase.util.LogInfo;
import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.*;

@Slf4j
@Service
public class BannerServiceImpl implements BannerService {

    @Autowired
    private BannerMapper bannerMapper;

    @Override
    public ResponseForm getBrandList(RequestForm param) {
        ResponseForm result = new ResponseForm();
        try {
            List<Banner> banners = bannerMapper.selectList();
            result.setData(banners);
        } catch (Exception e) {
            result.setStatus(false);
            result.setMessage(LogInfo.ERROR);
            log.error("查询轮播图列表error", e);
        }
        return result;
    }

    @Override
    public ResponseForm addBanner(RequestForm param) {
        ResponseForm result = new ResponseForm();
        Map<String, Object> paramMap = (Map<String, Object>) param.getData();
        try {
            String name = (String) paramMap.get("name");
            String url = (String) paramMap.get("url");
            if (!StringUtils.isNotBlank(name)
                    || !StringUtils.isNotBlank(url)) {
                result.setStatus(false);
                result.setMessage(LogInfo.PARAM_ERROR);
                log.error(LogInfo.PARAM_ERROR);
                return result;
            }

            bannerMapper.insertOne(paramMap);
        } catch (Exception e) {
            result.setStatus(false);
            result.setMessage(LogInfo.ERROR);
            log.error("创建轮播图error", e);
        }
        return result;
    }

    @Override
    public ResponseForm editBrand(RequestForm param) {
        ResponseForm result = new ResponseForm();
        Map<String, Object> paramMap = (Map<String, Object>) param.getData();
        try {
            bannerMapper.updateById(paramMap);
        } catch (Exception e) {
            result.setStatus(false);
            result.setMessage(LogInfo.ERROR);
            log.error("修改轮播图error", e);
        }
        return result;
    }

    @Override
    public ResponseForm getBannerById(RequestForm param) {
        ResponseForm result = new ResponseForm();
        Map<String, Object> paramMap = (Map<String, Object>) param.getData();
        try {
            Banner banner = bannerMapper.selectById(Integer.valueOf(String.valueOf(paramMap.get("id"))));
            result.setData(banner);
        } catch (Exception e) {
            result.setStatus(false);
            result.setMessage(LogInfo.ERROR);
            log.error("查询轮播图error", e);
        }
        return result;
    }

    @Override
    public ResponseForm delBanner(RequestForm param) {
        ResponseForm result = new ResponseForm();
        Map<String, Object> paramMap = (Map<String, Object>) param.getData();
        try {
            String id = (String) paramMap.get("id");
            if (!StringUtils.isNotBlank(id)) {
                result.setStatus(false);
                result.setMessage(LogInfo.PARAM_ERROR);
                log.error(LogInfo.PARAM_ERROR);
                return result;
            }

            bannerMapper.delBanner(Integer.valueOf(id));
        } catch (Exception e) {
            result.setStatus(false);
            result.setMessage(LogInfo.ERROR);
            log.error("删除轮播图error", e);
        }
        return result;
    }

}
