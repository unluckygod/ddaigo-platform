package com.purchase.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

import com.purchase.mapper.AreaMapper;
import com.purchase.service.AreaService;
import com.purchase.util.AuthUtil;
import com.purchase.util.LogInfo;
import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AreaServiceImpl implements AreaService {

	@Resource
	AreaMapper areaMapper;

	@Override
	public ResponseForm getAreaByPid(RequestForm param,HttpServletRequest request) {
		ResponseForm result = new ResponseForm();
		Map<String, Object> paramMap = (Map<String, Object>) param.getData();
		try {
			
			try {
				 AuthUtil.getUserId(request);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				result.setStatus(false);
				result.setCode("500");
				result.setMessage(LogInfo.AUTH_ERROR);
				log.error(LogInfo.AUTH_ERROR);
				return result;
			}
			
			List<Map<String, Object>> list = areaMapper.getAreaByPid(paramMap);
//			Example example = new Example(Area.class);
//			Criteria c = example.createCriteria();
//			c.andEqualTo("pid","0" );
//			Area area = new Area();
//			area.setPid(0);
//			List<Area> list =areaMapper.select(area);
			result.setData(list);
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error(LogInfo.ERROR);
		}
		return result;
	}
}
