package com.purchase.service;

import javax.servlet.http.HttpServletRequest;

import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

public interface StoreActivityService {

	
	/**
	 * 获取活动列表
	 * @param ResponseForm
	 * @return ResponseForm
	 */
	ResponseForm activityList(RequestForm param,HttpServletRequest request);

	/**
	 * 获取单个活动
	 * @param ResponseForm
	 * @return ResponseForm
	 */
	ResponseForm getActivityDetail(RequestForm param, HttpServletRequest request);

	/**
	 * 新增活动
	 * @param ResponseForm
	 * @return
	 */
	ResponseForm addActivity(RequestForm param, HttpServletRequest request);

	
	/**
	 * 修改活动
	 * @param ResponseForm
	 * @return
	 */
	ResponseForm editActivity(RequestForm param, HttpServletRequest request);

	/**
	 * 获取活动商品列表
	 * @param ResponseForm
	 * @return
	 */
	ResponseForm getActivityGoodsList(RequestForm param, HttpServletRequest request);

	/**
	 * 搜索活动商品列表
	 * @param ResponseForm
	 * @return
	 */
	ResponseForm searchActivityGoodsList(RequestForm param, HttpServletRequest request);
	/**
	 * 获取活动列表后台使用
	 * @param ResponseForm
	 * @return ResponseForm
	 */
	ResponseForm getactivityListAdmin(RequestForm param, HttpServletRequest request);

	/**
	 * 查所有活动的列表
	 * @param ResponseForm
	 * @return ResponseForm
	 */
	ResponseForm getAllActivityListAdmin(RequestForm param, HttpServletRequest request);
	
	
	/**
	 * 首页活动列表
	 * @param ResponseForm
	 * @return
	 */
	ResponseForm activityIndex(RequestForm param, HttpServletRequest request);

	/**
	 * 获取活动商品列表(带优惠信息)
	 * @param ResponseForm
	 * @return
	 */
	ResponseForm getActivityGoodsListWithCoupon(RequestForm param, HttpServletRequest request);

	
	/**
	 * 查询活动优惠券
	 * @param ResponseForm
	 * @return
	 */
	ResponseForm getActivityCoupon(RequestForm param, HttpServletRequest request);



}
