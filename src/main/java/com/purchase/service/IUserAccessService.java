/**
 * *************************************
 * Copyright(c)2016 Dingdang Yixia 
 * All Rights Reserved.
 *
 * @File: IWxUserAccessService.java
 * @Package: com.ddyx.service
 * @Description:
 * 
 * @Version: V1.0
 * @Author: Yidong.Xiang
 * @Date: 2017年12月12日 下午5:11:20
 * 
 * *************************************
 */
package com.purchase.service;

import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

/**
 * @ClassName: IWxUserAccessService.java
 * @Module: 
 * @Description: 
 * 
 * @author: Yidong.Xiang
 * @date: 2017年12月12日 下午5:11:20
 * 
 */
public interface IUserAccessService {

	public ResponseForm wechatUserlogin(RequestForm requestForm);
	
	public ResponseForm saveUserInfo(RequestForm requestForm);

	public ResponseForm getUserRole(RequestForm requestForm);

	public ResponseForm checkSession(RequestForm requestForm);
	/**
	 * 使用手机号获取叮店用户Id
	 * @param requestForm
	 * @return
	 */
	public ResponseForm getWxUid(RequestForm requestForm);
	
	/**
	 *	解密微信用户手机号
	 * @param requestForm
	 * @return
	 */
	public ResponseForm decodePhoneNumber(RequestForm requestForm);

	public ResponseForm authorize(RequestForm requestForm);

	public ResponseForm newSaveUserInfo(RequestForm requestForm);

	public ResponseForm authorize2(RequestForm requestForm);

	public ResponseForm saveUserInfo2(RequestForm requestForm);

	public ResponseForm pullPhone(RequestForm requestForm);
}
