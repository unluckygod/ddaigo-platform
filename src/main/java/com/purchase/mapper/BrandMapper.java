package com.purchase.mapper;

import com.purchase.entity.Brand;

import tk.mybatis.mapper.common.Mapper;

@MyBatisRepository
public interface BrandMapper extends Mapper<Brand>  {

}
