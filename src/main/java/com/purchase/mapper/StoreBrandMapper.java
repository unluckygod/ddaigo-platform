package com.purchase.mapper;

import com.purchase.entity.Brand;
import com.purchase.entity.StoreBrand;

import tk.mybatis.mapper.common.Mapper;

@MyBatisRepository
public interface StoreBrandMapper extends Mapper<StoreBrand>  {

}
