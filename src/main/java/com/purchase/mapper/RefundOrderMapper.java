package com.purchase.mapper;

import com.purchase.entity.RefundOrder;
import com.purchase.entity.RefundPay;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

@MyBatisRepository
public interface RefundOrderMapper extends Mapper<RefundOrder> {

    /**
     * 保存退款订单
     *
     * @param paramMap
     * @return
     */
    int saveRefundOrder(Map<String, Object> paramMap);

    /**
     * 修改退款订单
     *
     * @param paramMap
     * @return
     */
    int modifyRefundOrder(Map<String, Object> paramMap);

    /**
     * 根据 orderId 获取退款订单
     *
     * @param orderId
     * @return
     */
    Map<String, Object> getRefundOrderByOrderId(@Param("orderId") String orderId);

}
