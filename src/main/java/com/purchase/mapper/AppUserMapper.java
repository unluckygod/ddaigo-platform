package com.purchase.mapper;

import com.purchase.entity.AppUser;

import tk.mybatis.mapper.common.Mapper;

public interface AppUserMapper extends Mapper<AppUser>{

	Integer selectUserIdByOpenId(String openId);
	
}
