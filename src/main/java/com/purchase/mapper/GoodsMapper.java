package com.purchase.mapper;

import java.util.List;
import java.util.Map;

import com.purchase.entity.Goods;

import tk.mybatis.mapper.common.Mapper;

@MyBatisRepository
public interface GoodsMapper extends Mapper<Goods>  {

	
	
	 void conditionCount(Map<String,Object> param);
	
	 List<Map<String, Object>> rateCondition(Map<String, Object> paramMap);
	 
	 List<Map<String, Object>> sizeCondition(Map<String, Object> paramMap);
	 
	 List<Map<String, Object>> getStockGoodsList(Map<String, Object> paramMap);
	 
	 List<Map<String, Object>> getGoodsColourList(Map<String, Object> paramMap);
	 
	 void addRateCondition(String storeNo);
	 
	 void addSizeCondition(String storeNo);
	 
	 List<Map<String, Object>> searcheGoodsList(Map<String, Object> paramMap);

	List getHotGoods(Map paramMap);

	void editGoodsByPc(Map paramMap);
}
