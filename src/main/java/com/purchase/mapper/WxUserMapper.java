/**
 * *************************************
 * Copyright(c)2016 Dingdang Yixia 
 * All Rights Reserved.
 *
 * @File: WxUser.java
 * @Package: com.ddyx.mapper
 * @Description:
 * 
 * @Version: V1.0
 * @Author: Yidong.Xiang
 * @Date: 2017年12月14日 下午5:50:09
 * 
 * *************************************
 */
package com.purchase.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.purchase.entity.WxUser;

import tk.mybatis.mapper.common.Mapper;

/**
 * @ClassName: WxUserMapper.java
 * @Module: 
 * @Description: 
 * 
 * @author: liuhoujie
 * @date: 2017年12月14日 下午5:50:09
 * 
 */
public interface WxUserMapper extends Mapper<WxUser> {

	public Integer getUserRole(@Param("storeNo")String storeNo,@Param("wxUid")String wxUid);

	public Map<String, Object> getEmployInfo(String mobile);

	public void updateVerifyPhone(Map paramMap);

	public Integer checkUsedPhone(Map paramMap);

	/**
	 * @desc: 统计指定时间段内推广人推广的人数
	 * @param paramMap
	 * @return
	 */
	public List<Map> quantityStatistics(Map paramMap);
}
