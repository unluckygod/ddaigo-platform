package com.purchase.mapper;

import java.math.BigDecimal;

import org.apache.ibatis.annotations.Param;

import com.purchase.entity.AccountIncome;

import tk.mybatis.mapper.common.Mapper;

public interface AccountIncomeMapper extends Mapper<AccountIncome> {

	/**
	 * 更新app端代购佣金账户的购买奖金预计收益
	 * @param wxUid	微信用户ID
	 * @param bonusAmount	购买佣金
	 */
	void updatePreIncomeAmount(@Param("wxUid")String wxUid,@Param("bonusAmount")BigDecimal bonusAmount);
	
	/**
	 * 更新小程序端会员佣金账户的推荐奖金预计收益
	 * @param wxUid	微信用户ID
	 * @param awardAmount	推荐佣金
	 */
	void updateMemberPreIncomeAmount(@Param("wxUid")String wxUid,@Param("awardAmount")BigDecimal awardAmount);

	/**
	 * 更新APP端代购佣金账户的推荐奖金预计收益
	 * @param wxUid	微信用户ID
	 * @param awardAmount	推荐佣金
	 */
	void updateVendorPreIncomeAmount(@Param("wxUid")String wxUid,@Param("awardAmount")BigDecimal awardAmount);
	
	/**
	 * 获取订单节省金额
	 * @param orderId
	 * @return
	 */
	BigDecimal getOrderReduceAmount(@Param("orderId") String orderId);
}