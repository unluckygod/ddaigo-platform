package com.purchase.mapper;

import com.purchase.entity.Manager;

import tk.mybatis.mapper.common.Mapper;

public interface ManagerMapper extends Mapper<Manager> {

}