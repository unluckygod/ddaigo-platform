package com.purchase.mapper;

import java.util.List;
import java.util.Map;

import com.purchase.entity.StoreActivity;

import tk.mybatis.mapper.common.Mapper;

@MyBatisRepository
public interface StoreActivityMapper extends Mapper<StoreActivity>  {

	/**
	 * 获取活动列表
	 * @param ResponseForm
	 * @return
	 */
	List<StoreActivity> activityIndex(Map<String, Object> paramMap); 
	
	/**
	 * 获取活动商品列表
	 * @param ResponseForm
	 * @return
	 */
	List<Map<String, Object>> getActivityGoods(Map<String, Object> paramMap);
	
	
	/**
	 * 搜索活动商品列表
	 * @param ResponseForm
	 * @return
	 */
	List<Map<String, Object>> searchActivityGoods(Map<String, Object> paramMap);

//	/**
//	 * 定时开启活动
//	 */
//	void autoSwitchActivitiesStatus();


	List<Map> getActivities(Map paramMap);
}
