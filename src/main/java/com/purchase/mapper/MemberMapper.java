package com.purchase.mapper;

import com.purchase.entity.Member;

import tk.mybatis.mapper.common.Mapper;

public interface MemberMapper extends Mapper<Member> {

}