package com.purchase.mapper;

import com.purchase.entity.Vendor;

import tk.mybatis.mapper.common.Mapper;

public interface VendorMapper extends Mapper<Vendor> {

}