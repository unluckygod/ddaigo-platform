package com.purchase.mapper;

import com.purchase.entity.Banner;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

@MyBatisRepository
public interface BannerMapper extends Mapper<Banner> {

    int insertOne(Map<String, Object> paramMap);

    int delBanner(@Param("id")Integer id);

    List<Banner> selectList();

    int updateById(Map<String, Object> paramMap);

    Banner selectById(@Param("id")Integer id);

}
